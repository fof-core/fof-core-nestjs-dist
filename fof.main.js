"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const swagger_1 = require("@nestjs/swagger");
const swStats = require("swagger-stats");
// sleep time expects milliseconds
function sleep(time = 1000) {
    return new Promise((resolve) => setTimeout(resolve, time));
}
class iFofAppConfigOptions {
    constructor() {
        this.simulateLatencyInMs = 1000;
        this.swStatsActivate = false;
    }
}
exports.fofAppConfig = async (app, fofAppOptions) => {
    const serviceTechnicalName = app.get('ConfigService').get('service.technicalName');
    const serviceName = app.get('ConfigService').get('service.name');
    const rabbitMQUrl = app.get('ConfigService').get('rabbitMQ.url');
    // const microservice = app.connectMicroservice({
    //   transport: Transport.RMQ,
    //   options: {
    //     urls: [rabbitMQUrl],
    //     // queue: serviceTechnicalName,     
    //     noAck: false, 
    //     queue: 'fof-core',
    //     queueOptions: {
    //       durable: false
    //     },
    //   }
    // })
    if (fofAppOptions && fofAppOptions.simulateLatencyInMs) {
        // simulate latency
        app.use(async (req, res, next) => {
            await sleep(fofAppOptions.simulateLatencyInMs);
            next();
        });
    }
    const corsOptions = {
        origin: ($origin, cb) => {
            // accept all origins
            // toDo: accept clientWhiteList only: wait for API gw to do the job
            cb(null, true);
            // if (['http://my.url.in.space'].indexOf($origin) > -1) {
            //   cb(null, true);
            // }  else {
            //   cb(new Error('Not allowed by CORS'));
            // }
        },
        methods: "GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS",
        // allowedHeaders: ['Content-Type', 'Authorization'],
        credentials: true,
        preflightContinue: false,
        optionsSuccessStatus: 200
    };
    app.enableCors(corsOptions);
    const options = new swagger_1.DocumentBuilder()
        .addBearerAuth()
        .setTitle(`${serviceName} APIs`)
        // .setDescription(`Documentation for user and access management API`)
        .setDescription(`Documentation for ${serviceName} API`)
        .setVersion('0.1')
        // .addTag('Organizations', 'ma description<br>ici')
        .addTag('Auth/Basic')
        .addTag('Auth/Kerberos')
        .build();
    const document = swagger_1.SwaggerModule.createDocument(app, options, {
    // include: [AuthModule, PermissionsModule]
    });
    // SwaggerModule.setup('docs/iam', app, document)
    swagger_1.SwaggerModule.setup('docs/apis', app, document);
    // http://localhost:3000/swagger-stats/ui
    // https://swaggerstats.io/  
    if (fofAppOptions && fofAppOptions.swStatsActivate) {
        app.use(swStats.getMiddleware({
            name: serviceName,
            swaggerSpec: document
        }));
    }
    // await app.startAllMicroservicesAsync()  
};
//# sourceMappingURL=fof.main.js.map
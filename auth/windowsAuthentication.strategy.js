"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const passport_1 = require("@nestjs/passport");
const common_1 = require("@nestjs/common");
const WindowsStrategy = require("passport-windowsauth");
const core_interface_1 = require("../core/core.interface");
const fof_configuration_1 = require("../core/fof.configuration");
let WindowsAuthenticationStrategy = class WindowsAuthenticationStrategy extends passport_1.PassportStrategy(WindowsStrategy) {
    constructor(config) {
        // https://github.com/ldapjs/node-ldapjs/issues/403
        // LDAP connection error: Error: read ECONNRESET
        //     at TCP.onStreamRead (internal/stream_base_commons.js:201:27) {
        //   errno: 'ECONNRESET',
        //   code: 'ECONNRESET',
        //   syscall: 'read'
        // }
        super({
            integrated: true,
            ldap: {
                url: fof_configuration_1.fofConfig().authentication.ldapConnexion.url,
                base: fof_configuration_1.fofConfig().authentication.ldapConnexion.base,
                bindDN: fof_configuration_1.fofConfig().authentication.ldapConnexion.bindDN,
                bindCredentials: fof_configuration_1.fofConfig().authentication.ldapConnexion.bindCredentials,
                reconnect: {
                    initialDelay: 100,
                    maxDelay: 1000,
                    failAfter: 10
                }
            }
        });
        this.config = config;
    }
    async validate(payload) {
        return payload;
    }
};
WindowsAuthenticationStrategy = __decorate([
    common_1.Injectable(),
    __param(0, common_1.Inject(core_interface_1.FOF_CORE_OPTIONS)),
    __metadata("design:paramtypes", [Object])
], WindowsAuthenticationStrategy);
exports.WindowsAuthenticationStrategy = WindowsAuthenticationStrategy;
//# sourceMappingURL=windowsAuthentication.strategy.js.map
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const openapi = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
const auth_guard_1 = require("./auth.guard");
const auth_service_1 = require("./auth.service");
const express = require("express");
const swagger_1 = require("@nestjs/swagger");
const user_interface_1 = require("../shared/user.interface");
let AuthController = class AuthController {
    constructor(fofAuthService) {
        this.fofAuthService = fofAuthService;
    }
    async login(session, req) {
        return this.fofAuthService.userAppCookieAndJwtCreate(req.user, session);
    }
    // @UseGuards(FofAuthJwtGuard) 
    async logOut(res) {
        return this.fofAuthService.logOut(res);
    }
    async getUserFromCookie(session) {
        return this.fofAuthService.getUserFromCookie(session);
    }
    async winLogin(req
    // @Query('allUsers') allUsers: boolean
    ) {
        const userInfo = await this.fofAuthService.windowsLoginUserParse(req);
        return userInfo;
    }
    async winUserValidate(session, req) {
        return this.fofAuthService.userAppCookieAndJwtCreate(req.user, session);
    }
};
__decorate([
    swagger_1.ApiTags('Auth/Basic'),
    swagger_1.ApiOperation({
        summary: 'Basic authentication',
        description: ``
    }),
    swagger_1.ApiBody({
        description: 'username and password',
        type: user_interface_1.iUserAuth
    }),
    common_1.UseGuards(auth_guard_1.FofAuthLogin),
    common_1.Post('login'),
    openapi.ApiResponse({ status: 201, type: require("../shared/user.interface").iUser }),
    __param(0, common_1.Session()), __param(1, common_1.Request()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "login", null);
__decorate([
    swagger_1.ApiTags('Auth/Basic'),
    swagger_1.ApiOperation({
        summary: 'Logout',
        description: ``
    }),
    common_1.Get('logout'),
    openapi.ApiResponse({ status: 200 }),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "logOut", null);
__decorate([
    swagger_1.ApiTags('Auth/Basic'),
    swagger_1.ApiOperation({
        summary: 'Exchange cookie for the user profile anf get a new one',
        description: `The getUser endpoint call must have a cookie attached to the call to work.<br>
    The cookie is setup on the /auth path only when calling the 'login' or 'winLogin' endpoints.`
    }),
    common_1.Get('getUser'),
    openapi.ApiResponse({ status: 200, type: require("../shared/user.interface").iUser }),
    __param(0, common_1.Session()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "getUserFromCookie", null);
__decorate([
    swagger_1.ApiTags('Auth/Kerberos'),
    swagger_1.ApiOperation({
        summary: 'Windows Authentication SSO',
        description: `The service must run within IIS for being active since it uses Kerberos.<br>
    Have a look on [iisnode](https://github.com/Azure/iisnode) for more detail`
    }),
    common_1.UseGuards(auth_guard_1.FofWinAuthGuard),
    common_1.Get('winlogin'),
    openapi.ApiResponse({ status: 200 }),
    __param(0, common_1.Request()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "winLogin", null);
__decorate([
    swagger_1.ApiTags('Auth/Kerberos'),
    swagger_1.ApiOperation({
        summary: 'Validate a user against the App',
        description: `Use the windows token to map with the App user.`
    }),
    common_1.UseGuards(auth_guard_1.FofAuthWinAllUsersGuard),
    common_1.Get('winUserValidate'),
    openapi.ApiResponse({ status: 200, type: require("../shared/user.interface").iUser }),
    __param(0, common_1.Session()), __param(1, common_1.Request()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "winUserValidate", null);
AuthController = __decorate([
    common_1.Controller('auth'),
    __metadata("design:paramtypes", [auth_service_1.FofAuthService])
], AuthController);
exports.AuthController = AuthController;
//# sourceMappingURL=auth.controller.js.map
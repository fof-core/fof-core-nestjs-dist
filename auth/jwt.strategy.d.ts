import { Strategy } from 'passport-jwt';
import { FofAuthService } from './auth.service';
declare const JwtStrategy_base: new (...args: any[]) => Strategy;
export declare class JwtStrategy extends JwtStrategy_base {
    private fofAuthService;
    constructor(fofAuthService: FofAuthService);
    /**
     * the payload contain only the userId in addition to iat & expiracy
     * setted in ./local.strategy -> validate or win..strategy etc
     * @param payload
     */
    validate(payload: any): Promise<false | import("..").iUser>;
}
export {};

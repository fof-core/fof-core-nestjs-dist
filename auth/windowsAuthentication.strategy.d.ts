import { FofHardcodedOptions } from '../core/core.interface';
declare const WindowsAuthenticationStrategy_base: new (...args: any[]) => any;
export declare class WindowsAuthenticationStrategy extends WindowsAuthenticationStrategy_base {
    private config;
    constructor(config: FofHardcodedOptions);
    validate(payload: any): Promise<any>;
}
export {};

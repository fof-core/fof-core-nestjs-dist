import { Strategy } from 'passport-local';
import { FofAuthService } from './auth.service';
declare const LocalStrategy_base: new (...args: any[]) => Strategy;
export declare class LocalStrategy extends LocalStrategy_base {
    private readonly fofAuthService;
    constructor(fofAuthService: FofAuthService);
    validate(username: string, password: string): Promise<any>;
}
export {};

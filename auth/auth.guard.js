"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
const passport_1 = require("@nestjs/passport");
/**
 * Guard without authentication but set req.user with login / password
 * @public
 */
exports.FofAuthLogin = passport_1.AuthGuard('local');
/**
 * Guard for Fof API security (JWT)
 * @public
 */
exports.FofAuthJwtGuard = passport_1.AuthGuard('jwt');
__export(require("./auth.jwt-win.guard"));
/**
 * Guard for Fof Win AUTH API security. To use only when the service is running within IIS.
 * @public
 */
exports.FofWinAuthGuard = passport_1.AuthGuard('WindowsAuthentication');
//# sourceMappingURL=auth.guard.js.map
import { FofAuthService } from './auth.service';
import * as express from 'express';
import { iUser } from '../shared/user.interface';
export declare class AuthController {
    private readonly fofAuthService;
    constructor(fofAuthService: FofAuthService);
    login(session: any, req: any): Promise<iUser>;
    logOut(res: express.Response): Promise<void>;
    getUserFromCookie(session: any): Promise<iUser>;
    winLogin(req: express.Request): Promise<{
        user: iUser;
        accessToken: string;
        winUserToClean: any;
    }>;
    winUserValidate(session: any, req: any): Promise<iUser>;
}

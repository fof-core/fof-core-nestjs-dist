import { Strategy } from 'passport-jwt';
import { FofAuthService } from './auth.service';
import { FofHardcodedOptions } from '../core/core.interface';
declare const JwtWinStrategy_base: new (...args: any[]) => Strategy;
export declare class JwtWinStrategy extends JwtWinStrategy_base {
    private fofAuthService;
    private config;
    constructor(fofAuthService: FofAuthService, config: FofHardcodedOptions);
    /**
     * the payload contain only the userId in addition to iat & expiracy
     * setted in ./local.strategy -> validate or win..strategy etc
     * @param payload
     */
    validate(payload: any): Promise<any>;
}
export {};

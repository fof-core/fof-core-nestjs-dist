import { HttpService } from '@nestjs/common';
import { UserService } from '../permissions/services/user.service';
import { JwtService } from '@nestjs/jwt';
import { FofUser } from '../permissions/entities/user.entity';
import { Repository } from 'typeorm';
import { CacheService } from '../shared/cache.service';
import { iUser } from '../shared/user.interface';
import { FofHardcodedOptions } from '../core/core.interface';
import { Response, Request } from 'express';
export declare class FofAuthService {
    private readonly usersService;
    private readonly jwtService;
    private readonly userRepository;
    private readonly cacheService;
    private readonly httpService;
    private config;
    constructor(usersService: UserService, jwtService: JwtService, userRepository: Repository<FofUser>, cacheService: CacheService, httpService: HttpService, config: FofHardcodedOptions);
    /** validate credential for basic authentication (local strategy)  */
    userCredentialValidate(username: string, password: string): Promise<any>;
    /** get a user and roles from database and cache it */
    getUserAndAuthorizationById(userId: string, userLogin?: string): Promise<iUser>;
    /** get the user uid from cookie and send it's profil and a jwt
     * @remarq
     * Allow client App to not have to store the jwt somewhere
     */
    getUserFromCookie(session: any): Promise<iUser>;
    /** allow to log out by clearing the cookie */
    logOut(res: Response): Promise<void>;
    /** parse a ldap profile to extract user info and MS domain groups */
    windowsLoginUserParse(req: Request): Promise<{
        user: iUser;
        accessToken: string;
        winUserToClean: any;
    }>;
    userAppCookieAndJwtCreate(user: FofUser, session: any): Promise<iUser>;
}

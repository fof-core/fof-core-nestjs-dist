/**
 * Guard without authentication but set req.user with login / password
 * @public
 */
export declare const FofAuthLogin: import("@nestjs/passport").Type<import("@nestjs/passport").IAuthGuard>;
/**
 * Guard for Fof API security (JWT)
 * @public
 */
export declare const FofAuthJwtGuard: import("@nestjs/passport").Type<import("@nestjs/passport").IAuthGuard>;
export * from './auth.jwt-win.guard';
/**
 * Guard for Fof Win AUTH API security. To use only when the service is running within IIS.
 * @public
 */
export declare const FofWinAuthGuard: import("@nestjs/passport").Type<import("@nestjs/passport").IAuthGuard>;

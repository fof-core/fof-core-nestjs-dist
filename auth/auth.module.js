"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var AuthModule_1;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const core_interface_1 = require("../core/core.interface");
const auth_service_1 = require("./auth.service");
const local_strategy_1 = require("./local.strategy");
const jwt_strategy_1 = require("./jwt.strategy");
const jwt_win_strategy_1 = require("./jwt-win.strategy");
const windowsAuthentication_strategy_1 = require("./windowsAuthentication.strategy");
const permissions_module_1 = require("../permissions/permissions.module");
const passport_1 = require("@nestjs/passport");
const jwt_1 = require("@nestjs/jwt");
const typeorm_1 = require("@nestjs/typeorm");
const user_entity_1 = require("../permissions/entities/user.entity");
const auth_controller_1 = require("./auth.controller");
const fof_configuration_1 = require("../core/fof.configuration");
const core_constants_1 = require("../core/core.constants");
let AuthModule = AuthModule_1 = class AuthModule {
    static forRoot(options) {
        let authType;
        if (fof_configuration_1.fofConfig().authentication) {
            authType = fof_configuration_1.fofConfig().authentication.type;
        }
        const typeModuleOptions = {
            provide: core_interface_1.FOF_CORE_OPTIONS,
            useValue: options,
        };
        const imports = [
            permissions_module_1.PermissionsModule.forRoot(options),
            passport_1.PassportModule,
            common_1.HttpModule,
            jwt_1.JwtModule.register({
                secret: fof_configuration_1.fofConfig().secret,
                //toDo: change refresh / experire policy
                // implement token rotation
                signOptions: { expiresIn: fof_configuration_1.fofConfig().jwt.expiresIn },
            }),
            typeorm_1.TypeOrmModule.forFeature([
                user_entity_1.FofUser
            ], core_constants_1.FOF_DEFAULT_CONNECTION)
        ];
        const providers = [
            auth_service_1.FofAuthService,
            jwt_strategy_1.JwtStrategy,
            typeModuleOptions,
            jwt_win_strategy_1.JwtWinStrategy
        ];
        const _exports = [
            auth_service_1.FofAuthService,
            jwt_strategy_1.JwtStrategy,
            jwt_win_strategy_1.JwtWinStrategy
        ];
        let AuthTypeFounded = false;
        if (authType) {
            if (authType == core_interface_1.eAuth.windows) {
                providers.push(windowsAuthentication_strategy_1.WindowsAuthenticationStrategy);
                _exports.push(windowsAuthentication_strategy_1.WindowsAuthenticationStrategy);
                AuthTypeFounded = true;
            }
        }
        if (!AuthTypeFounded) {
            providers.push(local_strategy_1.LocalStrategy);
            _exports.push(local_strategy_1.LocalStrategy);
        }
        return {
            module: AuthModule_1,
            imports: imports,
            providers: providers,
            controllers: [
                auth_controller_1.AuthController
            ],
            exports: _exports
        };
    }
};
AuthModule = AuthModule_1 = __decorate([
    common_1.Module({})
], AuthModule);
exports.AuthModule = AuthModule;
//# sourceMappingURL=auth.module.js.map
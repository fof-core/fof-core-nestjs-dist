"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const user_service_1 = require("../permissions/services/user.service");
const jwt_1 = require("@nestjs/jwt");
const user_entity_1 = require("../permissions/entities/user.entity");
const typeorm_1 = require("typeorm");
const typeorm_2 = require("@nestjs/typeorm");
const cache_service_1 = require("../shared/cache.service");
const typeorm_3 = require("typeorm");
const core_interface_1 = require("../core/core.interface");
const fof_configuration_1 = require("../core/fof.configuration");
const core_constants_1 = require("../core/core.constants");
let FofAuthService = class FofAuthService {
    constructor(usersService, jwtService, userRepository, cacheService, httpService, config) {
        this.usersService = usersService;
        this.jwtService = jwtService;
        this.userRepository = userRepository;
        this.cacheService = cacheService;
        this.httpService = httpService;
        this.config = config;
    }
    /** validate credential for basic authentication (local strategy)  */
    async userCredentialValidate(username, password) {
        let wUser = await this.userRepository.findOne({ where: { email: username }, select: ['id', 'password', 'email'] });
        // null prevent for going through a auth guard
        if (!wUser) {
            return null;
        }
        let passwordOk = await wUser.passwordValidate(password);
        if (passwordOk) {
            const { password } = wUser, result = __rest(wUser, ["password"]);
            return result;
        }
        else {
            return null;
        }
    }
    /** get a user and roles from database and cache it */
    async getUserAndAuthorizationById(userId, userLogin) {
        const userCacheKey = `user_${userId}`;
        let userCached = await this.cacheService.get(userCacheKey);
        if (!userCached) {
            // Get the user in database
            let wUserQuery = typeorm_3.getRepository(user_entity_1.FofUser, core_constants_1.FOF_DEFAULT_CONNECTION)
                .createQueryBuilder("user")
                .leftJoinAndSelect("user.userRoleOrganizations", "userRoleOrganization")
                .leftJoinAndSelect("userRoleOrganization.role", "role")
                .leftJoinAndSelect("role.rolePermissions", "rolePermission")
                .leftJoinAndSelect("rolePermission.permission", "permission");
            if (userLogin) {
                // the user is authentified in an AD intranet 
                wUserQuery = wUserQuery
                    .where("user.login = :login", { login: userLogin });
            }
            else {
                // the user is authentified with a regular token
                wUserQuery = wUserQuery
                    .where("user.id = :id", { id: userId });
            }
            const wUser = await wUserQuery.getOne();
            // flat user roles permissions before caching him    
            const userPermissions = [];
            if (!wUser) {
                throw new common_1.UnauthorizedException('user_not_found_in_app');
            }
            if (wUser.userRoleOrganizations) {
                wUser.userRoleOrganizations.forEach(userRoleOrganization => {
                    userRoleOrganization.role.rolePermissions.forEach(rolePermission => {
                        if (userPermissions.filter(up => up === rolePermission.permission.code).length === 0) {
                            userPermissions.push(rolePermission.permission.code);
                        }
                    });
                });
            }
            userCached = wUser;
            // we don't need since we already flatted it
            userCached['userRoleOrganizations'] = undefined;
            userCached.permissions = userPermissions;
            // toDO: work on cache invalidation -> when add action in roles etc
            // this.cacheService.set(userCacheKey, userCached)
        }
        return userCached;
    }
    /** get the user uid from cookie and send it's profil and a jwt
     * @remarq
     * Allow client App to not have to store the jwt somewhere
     */
    async getUserFromCookie(session) {
        const accessToken = session.accessToken;
        if (!accessToken) {
            return null;
        }
        const userToken = this.jwtService.decode(accessToken);
        if (!userToken) {
            return null;
        }
        const cachedUser = await this.getUserAndAuthorizationById(userToken.userId);
        if (!cachedUser) {
            return null;
        }
        const payload = {
            userId: cachedUser.id
        };
        cachedUser.accessToken = this.jwtService.sign(payload);
        session.accessToken = cachedUser.accessToken;
        return cachedUser;
    }
    /** allow to log out by clearing the cookie */
    async logOut(res) {
        res.clearCookie(fof_configuration_1.fofConfig().service.technicalName, { path: 'auth' });
        res.clearCookie(`${fof_configuration_1.fofConfig().service.technicalName}.sig`, { path: 'auth' });
        res.json({ ok: 1 });
    }
    /** parse a ldap profile to extract user info and MS domain groups */
    async windowsLoginUserParse(req) {
        const winUser = req.user;
        let mainEmail = 'noEmail@founded.org';
        let login = 'noActiveDirectoryLogin';
        let groups = [];
        let distinguishedName = {};
        const parseADLine = (lineToParse) => {
            let name = '';
            let OU = [];
            let DC = [];
            const blocks = lineToParse.split(',');
            blocks.forEach((block) => {
                const blockDetails = block.split('=');
                const label = blockDetails[0];
                const value = blockDetails[1];
                switch (label) {
                    case 'CN':
                        name = value;
                        break;
                    case 'OU':
                        OU.push(value);
                        break;
                    case 'DC':
                        DC.push(value);
                        break;
                    default:
                        break;
                }
            });
            return {
                name: name,
                OU: OU,
                DC: DC
            };
        };
        if (!winUser) {
            return null;
        }
        if (winUser.emails.length > 0) {
            mainEmail = winUser.emails[0].value;
            if (mainEmail) {
                mainEmail = mainEmail.toLowerCase();
            }
        }
        if (winUser._json) {
            login = winUser._json.sAMAccountName;
            if (winUser._json.memberOf) {
                groups = winUser._json.memberOf.map((group) => {
                    return parseADLine(group);
                });
            }
            if (winUser._json.distinguishedName) {
                distinguishedName = parseADLine(winUser._json.distinguishedName);
            }
        }
        let firstName;
        let lastName;
        if (winUser.name) {
            firstName = winUser.name.givenName;
            lastName = winUser.name.familyName;
        }
        const payload = {
            windowAuthentification: true,
            id: winUser.id,
            fullName: winUser.displayName,
            firstName: firstName,
            lastName: lastName,
            email: mainEmail,
            login: login,
        };
        return {
            user: payload,
            accessToken: this.jwtService.sign(payload),
            winUserToClean: winUser
        };
    }
    async userAppCookieAndJwtCreate(user, session) {
        const cachedUser = await this.getUserAndAuthorizationById(user.id.toString(), user.login);
        const payload = {
            userId: cachedUser.id
        };
        /*
         * Send user with the access token for in memory use in front end
         * and set it in session too for generating a cookie
         * the cookie will be used for getting again the user
         * without storing it somewhere in the browser
         */
        cachedUser.accessToken = this.jwtService.sign(payload);
        session.accessToken = cachedUser.accessToken;
        return cachedUser;
    }
};
FofAuthService = __decorate([
    common_1.Injectable(),
    __param(2, typeorm_2.InjectRepository(user_entity_1.FofUser, core_constants_1.FOF_DEFAULT_CONNECTION)),
    __param(5, common_1.Inject(core_interface_1.FOF_CORE_OPTIONS)),
    __metadata("design:paramtypes", [user_service_1.UserService,
        jwt_1.JwtService,
        typeorm_1.Repository,
        cache_service_1.CacheService,
        common_1.HttpService, Object])
], FofAuthService);
exports.FofAuthService = FofAuthService;
//# sourceMappingURL=auth.service.js.map
import { DynamicModule } from '@nestjs/common';
import { FofHardcodedOptions } from '../core/core.interface';
export declare class AuthModule {
    static forRoot(options: FofHardcodedOptions): DynamicModule;
}

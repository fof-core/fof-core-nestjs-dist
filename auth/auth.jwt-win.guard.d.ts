import { ExecutionContext } from '@nestjs/common';
declare const FofAuthWinAllUsersGuard_base: import("@nestjs/passport").Type<import("@nestjs/passport").IAuthGuard>;
/** Guard for Fof API security only for win auth token. Authorize all MS domain users */
export declare class FofAuthWinAllUsersGuard extends FofAuthWinAllUsersGuard_base {
    canActivate(context: ExecutionContext): boolean | Promise<boolean> | import("rxjs").Observable<boolean>;
    handleRequest(err: any, user: any, info: any): any;
}
export {};

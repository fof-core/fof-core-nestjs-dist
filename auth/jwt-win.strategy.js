"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const passport_jwt_1 = require("passport-jwt");
const passport_1 = require("@nestjs/passport");
const common_1 = require("@nestjs/common");
const auth_service_1 = require("./auth.service");
const core_interface_1 = require("../core/core.interface");
const fof_configuration_1 = require("../core/fof.configuration");
let JwtWinStrategy = class JwtWinStrategy extends passport_1.PassportStrategy(passport_jwt_1.Strategy, 'winJwt') {
    constructor(fofAuthService, config) {
        super({
            jwtFromRequest: passport_jwt_1.ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: fof_configuration_1.fofConfig().secret,
        });
        this.fofAuthService = fofAuthService;
        this.config = config;
        // // it's not a basic auth
        // if (this.config.authentication) {
        //   this.clientWhiteList = this.config.authentication.clientWhiteList
        // }        
    }
    /**
     * the payload contain only the userId in addition to iat & expiracy
     * setted in ./local.strategy -> validate or win..strategy etc
     * @param payload
     */
    async validate(payload) {
        // the user is authentified winthin MS Domain
        if (payload.windowAuthentification) {
            // all domain users are authorized
            return payload;
        }
        return false;
    }
};
JwtWinStrategy = __decorate([
    common_1.Injectable(),
    __param(1, common_1.Inject(core_interface_1.FOF_CORE_OPTIONS)),
    __metadata("design:paramtypes", [auth_service_1.FofAuthService, Object])
], JwtWinStrategy);
exports.JwtWinStrategy = JwtWinStrategy;
//# sourceMappingURL=jwt-win.strategy.js.map
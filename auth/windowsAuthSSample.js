"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.winUserTest = {
    "user": {
        "userId": "055712fc-6605-4a5d-80db-6a74c8c66fb7",
        "username": "Fourteau Frederic",
        "email": "frederic.fourteau@avasad.ch",
        "login": "ffourteau",
        "groups": [
            {
                "name": "G_RAI_AllUsers_T",
                "OU": [
                    "RAI",
                    "0-Groups",
                    "INFR"
                ],
                "DC": [
                    "prod",
                    "omsv",
                    "ch"
                ]
            },
            {
                "name": "SFN-GEN-SYNC",
                "OU": [
                    "Transversal",
                    "U_Groups",
                    "ORG"
                ],
                "DC": [
                    "prod",
                    "omsv",
                    "ch"
                ]
            },
            {
                "name": "PWD_ORG",
                "OU": [
                    "Groups_system"
                ],
                "DC": [
                    "prod",
                    "omsv",
                    "ch"
                ]
            },
            {
                "name": "DEFAUT-Intranet AVASAD",
                "OU": [
                    "INTR-AVASAD-PROD",
                    "JCMS",
                    "0-Groups",
                    "INFR"
                ],
                "DC": [
                    "prod",
                    "omsv",
                    "ch"
                ]
            },
            {
                "name": "VPN_ENTREPRISE",
                "OU": [
                    "GEN",
                    "AVASAD",
                    "U_Groups",
                    "ORG"
                ],
                "DC": [
                    "prod",
                    "omsv",
                    "ch"
                ]
            },
            {
                "name": "G_Apollon_ST_Communications_Lecture_Seul",
                "OU": [
                    "Groups"
                ],
                "DC": [
                    "prod",
                    "omsv",
                    "ch"
                ]
            },
            {
                "name": "AIRWATCH_OPERATIONNEL",
                "OU": [
                    "Groups_system"
                ],
                "DC": [
                    "prod",
                    "omsv",
                    "ch"
                ]
            },
            {
                "name": "AIRWATCH_SSIT",
                "OU": [
                    "Groups_system"
                ],
                "DC": [
                    "prod",
                    "omsv",
                    "ch"
                ]
            },
            {
                "name": "G_SSIT_Users",
                "OU": [
                    "Groups"
                ],
                "DC": [
                    "prod",
                    "omsv",
                    "ch"
                ]
            },
            {
                "name": "ts_mindmanager",
                "OU": [
                    "Groups"
                ],
                "DC": [
                    "prod",
                    "omsv",
                    "ch"
                ]
            }
        ],
        "distinguishedName": {
            "name": "Fourteau Frederic",
            "OU": [
                "GENAVA",
                "AVASAD",
                "Users",
                "ORG"
            ],
            "DC": [
                "prod",
                "omsv",
                "ch"
            ]
        }
    },
    "winUserToClean": {
        "id": "055712fc-6605-4a5d-80db-6a74c8c66fb7",
        "displayName": "Fourteau Frederic",
        "name": {
            "familyName": "Fourteau",
            "givenName": "Frederic"
        },
        "emails": [
            {
                "value": "Frederic.Fourteau@avasad.ch"
            }
        ],
        "_json": {
            "dn": "CN=Fourteau Frederic,OU=GENAVA,OU=AVASAD,OU=Users,OU=ORG,DC=prod,DC=omsv,DC=ch",
            "controls": [],
            "objectClass": [
                "top",
                "person",
                "organizationalPerson",
                "user"
            ],
            "cn": "Fourteau Frederic",
            "sn": "Fourteau",
            "title": "AVASAD Informatique Consultant externe REDSEN",
            "description": "Consultant externe",
            "physicalDeliveryOfficeName": "Informatique",
            "givenName": "Frederic",
            "distinguishedName": "CN=Fourteau Frederic,OU=GENAVA,OU=AVASAD,OU=Users,OU=ORG,DC=prod,DC=omsv,DC=ch",
            "instanceType": "4",
            "whenCreated": "20191017083245.0Z",
            "whenChanged": "20200222163534.0Z",
            "displayName": "Fourteau Frederic",
            "uSNCreated": "158147150",
            "memberOf": [
                "CN=G_RAI_AllUsers_T,OU=RAI,OU=0-Groups,OU=INFR,DC=prod,DC=omsv,DC=ch",
                "CN=SFN-GEN-SYNC,OU=Transversal,OU=U_Groups,OU=ORG,DC=prod,DC=omsv,DC=ch",
                "CN=PWD_ORG,OU=Groups_system,DC=prod,DC=omsv,DC=ch",
                "CN=DEFAUT-Intranet AVASAD,OU=INTR-AVASAD-PROD,OU=JCMS,OU=0-Groups,OU=INFR,DC=prod,DC=omsv,DC=ch",
                "CN=VPN_ENTREPRISE,OU=GEN,OU=AVASAD,OU=U_Groups,OU=ORG,DC=prod,DC=omsv,DC=ch",
                "CN=G_Apollon_ST_Communications_Lecture_Seul,OU=Groups,DC=prod,DC=omsv,DC=ch",
                "CN=AIRWATCH_OPERATIONNEL,OU=Groups_system,DC=prod,DC=omsv,DC=ch",
                "CN=AIRWATCH_SSIT,OU=Groups_system,DC=prod,DC=omsv,DC=ch",
                "CN=G_SSIT_Users,OU=Groups,DC=prod,DC=omsv,DC=ch",
                "CN=ts_mindmanager,OU=Groups,DC=prod,DC=omsv,DC=ch"
            ],
            "uSNChanged": "178196541",
            "company": "REDSEN",
            "proxyAddresses": "SMTP:Frederic.Fourteau@avasad.ch",
            "homeMDB": "CN=DB020101-01,CN=Databases,CN=Exchange Administrative Group (FYDIBOHF23SPDLT),CN=Administrative Groups,CN=OMSV,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=prod,DC=omsv,DC=ch",
            "publicDelegatesBL": "CN=Chavannes Salle CSI-RA,OU=RESSOURCES,OU=AVASAD,DC=prod,DC=omsv,DC=ch",
            "mDBUseDefaults": "TRUE",
            "mailNickname": "ffourteau",
            "protocolSettings": [
                "HTTP§1§1§§§§§§",
                "OWA§1",
                "MAPI§1§§§§0§§§§§",
                "POP3§0§§§§§§§§§§§§",
                "IMAP4§0§§§§§§§§§§§§"
            ],
            "name": "Fourteau Frederic",
            "objectGUID": "055712fc-6605-4a5d-80db-6a74c8c66fb7",
            "userAccountControl": "512",
            "badPwdCount": "0",
            "codePage": "0",
            "countryCode": "0",
            "badPasswordTime": "132161347423692012",
            "lastLogon": "132270055089824714",
            "pwdLastSet": "132161160250988280",
            "primaryGroupID": "513",
            "objectSid": "\u0001\u0005\u0000\u0000\u0000\u0000\u0000\u0005\u0015\u0000\u0000\u0000�\u001f\u000fQ�\u0011�\r�|�\u0001(s\u0001\u0000",
            "accountExpires": "132540156000000000",
            "logonCount": "4609",
            "sAMAccountName": "ffourteau",
            "sAMAccountType": "805306368",
            "showInAddressBook": [
                "CN=Tous les utilisateurs,CN=All Address Lists,CN=Address Lists Container,CN=OMSV,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=prod,DC=omsv,DC=ch",
                "CN=All Users,CN=All Address Lists,CN=Address Lists Container,CN=OMSV,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=prod,DC=omsv,DC=ch",
                "CN=Default Global Address List,CN=All Global Address Lists,CN=Address Lists Container,CN=OMSV,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=prod,DC=omsv,DC=ch"
            ],
            "legacyExchangeDN": "/o=OMSV/ou=Exchange Administrative Group (FYDIBOHF23SPDLT)/cn=Recipients/cn=ba3111f4bdab4c91ae6c30a8b6a0ec45-Fourteau Frederic",
            "userPrincipalName": "ffourteau@prod.omsv.ch",
            "objectCategory": "CN=Person,CN=Schema,CN=Configuration,DC=prod,DC=omsv,DC=ch",
            "dSCorePropagationData": [
                "20200129153840.0Z",
                "20191220104652.0Z",
                "16010101000417.0Z"
            ],
            "lastLogonTimestamp": "132268629252175031",
            "textEncodedORAddress": "X400:C=CH;A= ;P=OMSV;O=Exchange;S=Fourteau;G=Frederic;",
            "mail": "Frederic.Fourteau@avasad.ch",
            "msExchHomeServerName": "/o=OMSV/ou=Exchange Administrative Group (FYDIBOHF23SPDLT)/cn=Configuration/cn=Servers/cn=AVD-SML020101",
            "msExchMailboxSecurityDescriptor": "\u0001\u0000\u0004�\u0014\u0000\u0000\u0000 \u0000\u0000\u0000\u0000\u0000\u0000\u0000,\u0000\u0000\u0000\u0001\u0001\u0000\u0000\u0000\u0000\u0000\u0005\n\u0000\u0000\u0000\u0001\u0001\u0000\u0000\u0000\u0000\u0000\u0005\n\u0000\u0000\u0000\u0004\u0000@\u0000\u0002\u0000\u0000\u0000\u0000\u0002\u0014\u0000\u0001\u0000\u0002\u0000\u0001\u0001\u0000\u0000\u0000\u0000\u0000\u0005\n\u0000\u0000\u0000\u0000\u0002$\u0000\u0001\u0000\u0000\u0000\u0001\u0005\u0000\u0000\u0000\u0000\u0000\u0005\u0015\u0000\u0000\u0000�\u001f\u000fQ�\u0011�\r�|�\u0001\u0005\t\u0001\u0000",
            "msExchUserAccountControl": "0",
            "msExchMailboxGuid": "��}���>H�БRgbc�",
            "msExchPoliciesExcluded": "{26491cfc-9e50-4857-861b-0cb8df22b5d7}",
            "msExchSafeSendersHash": "��\nD",
            "msExchOWAPolicy": "CN=Default,CN=OWA Mailbox Policies,CN=OMSV,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=prod,DC=omsv,DC=ch",
            "msExchMDBRulesQuota": "128",
            "msExchUMDtmfMap": [
                "emailAddress:3733374236878328",
                "lastNameFirstName:3687832837333742",
                "firstNameLastName:3733374236878328"
            ],
            "msExchUserCulture": "fr-CH",
            "msExchVersion": "88218628259840",
            "msExchELCMailboxFlags": "130",
            "avasadExchangeControl": "ACTIF",
            "msExchMobileMailboxFlags": "1",
            "msExchWhenMailboxCreated": "20191017090005.0Z",
            "msExchDelegateListLink": "CN=ExAllMBX,OU=Users,OU=Chuv,DC=prod,DC=omsv,DC=ch",
            "msExchDumpsterQuota": "31457280",
            "msExchRecipientDisplayType": "1073741824",
            "msExchArchiveQuota": "104857600",
            "msExchTextMessagingState": [
                "302120705",
                "16842751"
            ],
            "msExchDumpsterWarningQuota": "20971520",
            "msExchCalendarLoggingQuota": "6291456",
            "msExchArchiveWarnQuota": "94371840",
            "msExchRecipientTypeDetails": "1",
            "msExchRBACPolicyLink": "CN=AVASAD-Default role assignment policy,CN=Policies,CN=RBAC,CN=OMSV,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=prod,DC=omsv,DC=ch"
        }
    }
};
//# sourceMappingURL=windowsAuthSSample.js.map
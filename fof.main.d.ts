import { INestApplication } from '@nestjs/common';
declare abstract class iFofAppConfigOptions {
    abstract simulateLatencyInMs?: number;
    abstract swStatsActivate?: boolean;
}
export declare const fofAppConfig: (app: INestApplication, fofAppOptions?: iFofAppConfigOptions) => Promise<void>;
export {};

import { DynamicModule } from '@nestjs/common';
import { FofHardcodedOptions } from './core/core.interface';
export declare class FofCoreModule {
    static forRoot(options?: FofHardcodedOptions): DynamicModule;
}

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.dataUsers = [
    {
        userId: 1,
        // username: 'john',
        password: 'changeme',
        email: 'john@test.org',
        login: 'johnt'
    },
    {
        userId: 2,
        // username: 'chris',
        password: 'secret',
        email: 'chris@test.org',
        login: 'christ'
    },
    {
        userId: 3,
        // username: 'maria',
        password: 'guess',
        email: 'maria@test.org',
        login: 'mariat'
    },
];
//# sourceMappingURL=data.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var eP;
(function (eP) {
    eP["userCreate"] = "userCreate";
    eP["userUpdate"] = "userUpdate";
    eP["userRead"] = "userRead";
    eP["userDelete"] = "userDelete";
    eP["roleCreate"] = "roleCreate";
    eP["roleUpdate"] = "roleUpdate";
    eP["roleRead"] = "roleRead";
    eP["roleDelete"] = "roleDelete";
    eP["userRoleOrganizationCreate"] = "userRoleOrganizationCreate";
    eP["userRoleOrganizationUpdate"] = "userRoleOrganizationUpdate";
    eP["userRoleOrganizationRead"] = "userRoleOrganizationRead";
    eP["userRoleOrganizationDelete"] = "userRoleOrganizationDelete";
})(eP = exports.eP || (exports.eP = {}));
//# sourceMappingURL=core.permissions.js.map
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var PermissionsModule_1;
Object.defineProperty(exports, "__esModule", { value: true });
// module
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const shared_module_1 = require("../shared/shared.module");
const uservices_module_1 = require("../uservices/uservices.module");
// services 
const user_service_1 = require("./services/user.service");
const role_service_1 = require("./services/role.service");
const organization_service_1 = require("./services/organization.service");
const rolePermission_service_1 = require("./services/rolePermission.service");
const userRoleOrganization_service_1 = require("./services/userRoleOrganization.service");
const permission_service_1 = require("./services/permission.service");
const userUservice_service_1 = require("./services/userUservice.service");
// entities
const entities_1 = require("./entities");
// subscribers
const user_entity_subscriber_1 = require("./entities/user.entity.subscriber");
// controllers
const user_controller_1 = require("./controllers/user.controller");
const role_controller_1 = require("./controllers/role.controller");
const rolePermission_controller_1 = require("./controllers/rolePermission.controller");
const userRoleOrganization_controller_1 = require("./controllers/userRoleOrganization.controller");
const organization_controller_1 = require("./controllers/organization.controller");
const permission_controller_1 = require("./controllers/permission.controller");
const userUservice_controller_1 = require("./controllers/userUservice.controller");
// options & constants
const core_interface_1 = require("../core/core.interface");
const core_constants_1 = require("../core/core.constants");
let PermissionsModule = PermissionsModule_1 = class PermissionsModule {
    static forRoot(options) {
        const typeModuleOptions = {
            provide: core_interface_1.FOF_CORE_OPTIONS,
            useValue: options,
        };
        return {
            module: PermissionsModule_1,
            imports: [
                typeorm_1.TypeOrmModule.forFeature([
                    ...entities_1.permissionsEntities
                ], core_constants_1.FOF_DEFAULT_CONNECTION),
                shared_module_1.SharedModule,
                uservices_module_1.UservicesModule.forRoot(options)
            ],
            providers: [
                typeModuleOptions,
                user_service_1.UserService,
                role_service_1.RoleService,
                organization_service_1.OrganizationService,
                permission_service_1.PermissionService,
                rolePermission_service_1.RolePermissionService,
                userRoleOrganization_service_1.UserRoleOrganizationService,
                user_entity_subscriber_1.UserSubscriber,
                userUservice_service_1.userUserviceService,
            ],
            controllers: [
                role_controller_1.RoleController,
                rolePermission_controller_1.RolePermissionController,
                userRoleOrganization_controller_1.UserRoleOrganizationController,
                organization_controller_1.OrganizationController,
                user_controller_1.UserController,
                permission_controller_1.PermissionController,
                userUservice_controller_1.userUserviceController
            ],
            exports: [
                user_service_1.UserService,
                role_service_1.RoleService,
                organization_service_1.OrganizationService,
                permission_service_1.PermissionService,
                rolePermission_service_1.RolePermissionService,
                userRoleOrganization_service_1.UserRoleOrganizationService
            ]
        };
    }
};
PermissionsModule = PermissionsModule_1 = __decorate([
    common_1.Module({})
], PermissionsModule);
exports.PermissionsModule = PermissionsModule;
//# sourceMappingURL=permissions.module.js.map
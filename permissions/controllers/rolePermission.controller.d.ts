import { CrudController } from "@nestjsx/crud";
import { RolePermission } from "../entities/rolePermission.entitiy";
import { RolePermissionService } from "../services/rolePermission.service";
export declare class RolePermissionController implements CrudController<RolePermission> {
    service: RolePermissionService;
    constructor(service: RolePermissionService);
}

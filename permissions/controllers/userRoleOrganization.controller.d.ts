import { CrudController } from "@nestjsx/crud";
import { UserRoleOrganization } from "../entities/userRoleOrganization.entity";
import { UserRoleOrganizationService } from "../services/userRoleOrganization.service";
export declare class UserRoleOrganizationController implements CrudController<UserRoleOrganization> {
    service: UserRoleOrganizationService;
    constructor(service: UserRoleOrganizationService);
}

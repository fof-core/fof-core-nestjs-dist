"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const openapi = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
const uservice_entity_1 = require("../entities/uservice.entity");
const uservice_service_1 = require("../services/uservice.service");
const crud_decorators_1 = require("../../core/crud.decorators");
const crud_1 = require("@nestjsx/crud");
const fof_configuration_1 = require("../../core/fof.configuration");
const uservices_service_1 = require("../../uservices/uservices.service");
const uservices_interface_1 = require("../../uservices/uservices.interface");
const core_interface_1 = require("../../core/core.interface");
let UservicesController = class UservicesController {
    constructor(service, fofUservice, config) {
        this.service = service;
        this.fofUservice = fofUservice;
        this.config = config;
        this.isMasterForMiniServicesManagement = false;
        this.fofConfig = fof_configuration_1.fofConfig();
        this.isMasterForMiniServicesManagement = config.isMasterForUserManagement;
    }
    get base() {
        return this;
    }
    async createOne(req, dto) {
        const newUservice = await this.base.createOneBase(req, dto);
        if (newUservice && newUservice.id
            && this.isMasterForMiniServicesManagement && this.fofConfig.rabbitMQ.url) {
            this.fofUservice.publishTopicMessage(newUservice, uservices_interface_1.efofServiceTopic.fofUservice, uservices_interface_1.eFofServiceMethod.create);
        }
        return newUservice;
    }
    async updateOne(req, dto) {
        const newUservice = await this.base.updateOneBase(req, dto);
        if (newUservice && newUservice.id &&
            this.isMasterForMiniServicesManagement && this.fofConfig.rabbitMQ.url) {
            this.fofUservice.publishTopicMessage(newUservice, uservices_interface_1.efofServiceTopic.fofUservice, uservices_interface_1.eFofServiceMethod.update);
        }
        return newUservice;
    }
    async deleteOne(req, id) {
        const result = await this.base.deleteOneBase(req);
        if (id && this.isMasterForMiniServicesManagement &&
            this.fofConfig.rabbitMQ.url) {
            this.fofUservice.publishTopicMessage(id, uservices_interface_1.efofServiceTopic.fofUservice, uservices_interface_1.eFofServiceMethod.delete);
        }
        return result;
    }
};
__decorate([
    crud_1.Override(),
    openapi.ApiResponse({ status: 200, type: require("../entities/uservice.entity").FofuService }),
    __param(0, crud_1.ParsedRequest()),
    __param(1, crud_1.ParsedBody()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, uservice_entity_1.FofuService]),
    __metadata("design:returntype", Promise)
], UservicesController.prototype, "createOne", null);
__decorate([
    crud_1.Override(),
    openapi.ApiResponse({ status: 200, type: require("../entities/uservice.entity").FofuService }),
    __param(0, crud_1.ParsedRequest()),
    __param(1, crud_1.ParsedBody()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, uservice_entity_1.FofuService]),
    __metadata("design:returntype", Promise)
], UservicesController.prototype, "updateOne", null);
__decorate([
    crud_1.Override(),
    openapi.ApiResponse({ status: 200, type: Object }),
    __param(0, crud_1.ParsedRequest()),
    __param(1, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Number]),
    __metadata("design:returntype", Promise)
], UservicesController.prototype, "deleteOne", null);
UservicesController = __decorate([
    crud_decorators_1.FofCrud({
        entity: uservice_entity_1.FofuService,
        allowToJoin: []
    }),
    crud_decorators_1.FofSwagger('uServices'),
    common_1.Controller('uservices'),
    __param(2, common_1.Inject(core_interface_1.FOF_CORE_OPTIONS)),
    __metadata("design:paramtypes", [uservice_service_1.UserviceCrudService,
        uservices_service_1.FofUservice, Object])
], UservicesController);
exports.UservicesController = UservicesController;
//# sourceMappingURL=uservices.controller.js.map
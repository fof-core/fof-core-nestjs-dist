"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const openapi = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
const permission_entity_1 = require("../entities/permission.entity");
const permission_service_1 = require("../services/permission.service");
const crud_decorators_1 = require("../../core/crud.decorators");
let PermissionController = class PermissionController {
    constructor(service) {
        this.service = service;
    }
};
PermissionController = __decorate([
    crud_decorators_1.FofCrud({
        entity: permission_entity_1.Permission,
        allowToJoin: [
            { name: 'userPermissions' },
            { name: 'userPermissions.permission' }
        ]
    }),
    crud_decorators_1.FofSwagger('Permissions'),
    common_1.Controller('permissions'),
    __metadata("design:paramtypes", [permission_service_1.PermissionService])
], PermissionController);
exports.PermissionController = PermissionController;
//# sourceMappingURL=permission.controller.js.map
import { CrudController, CrudRequest } from "@nestjsx/crud";
import { Organization } from "../entities/organization.entity";
import { OrganizationService } from "../services/organization.service";
import { iUser } from '../../shared/user.interface';
export declare class OrganizationController implements CrudController<Organization> {
    service: OrganizationService;
    constructor(service: OrganizationService);
    get base(): CrudController<Organization>;
    getTReeview(user: iUser): Promise<Organization[]>;
    createOne(req: CrudRequest, dto: Organization): Promise<Organization>;
    deleteOne(req: CrudRequest): Promise<void | Organization>;
}

import { CrudController } from "@nestjsx/crud";
import { Permission } from "../entities/permission.entity";
import { PermissionService } from "../services/permission.service";
export declare class PermissionController implements CrudController<Permission> {
    service: PermissionService;
    constructor(service: PermissionService);
}

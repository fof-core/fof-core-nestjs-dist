"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const openapi = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
const role_entity_1 = require("../entities/role.entity");
const role_service_1 = require("../services/role.service");
const permission_enum_1 = require("../../shared/permission.enum");
const permissions_decorator_1 = require("../permissions.decorator");
const crud_decorators_1 = require("../../core/crud.decorators");
let RoleController = class RoleController {
    constructor(service) {
        this.service = service;
    }
    // Additionnal
    async test() {
        return { ok: 1 };
    }
};
__decorate([
    permissions_decorator_1.FofRBACPermissions(permission_enum_1.eCp.roleCreate, permission_enum_1.eCp.roleUpdate),
    common_1.Get('test'),
    openapi.ApiResponse({ status: 200 }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], RoleController.prototype, "test", null);
RoleController = __decorate([
    crud_decorators_1.FofCrud({
        entity: role_entity_1.Role,
        allowToJoin: [
            { name: 'rolePermissions' },
            { name: 'rolePermissions.permission' }
        ]
    }),
    crud_decorators_1.FofSwagger('Roles'),
    common_1.Controller('roles'),
    __metadata("design:paramtypes", [role_service_1.RoleService])
], RoleController);
exports.RoleController = RoleController;
//# sourceMappingURL=role.controller.js.map
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const openapi = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
const rolePermission_entitiy_1 = require("../entities/rolePermission.entitiy");
const rolePermission_service_1 = require("../services/rolePermission.service");
const crud_decorators_1 = require("../../core/crud.decorators");
let RolePermissionController = class RolePermissionController {
    constructor(service) {
        this.service = service;
    }
};
RolePermissionController = __decorate([
    crud_decorators_1.FofCrud({
        entity: rolePermission_entitiy_1.RolePermission
    }),
    crud_decorators_1.FofSwagger('RolePermissions'),
    common_1.Controller('rolePermissions'),
    __metadata("design:paramtypes", [rolePermission_service_1.RolePermissionService])
], RolePermissionController);
exports.RolePermissionController = RolePermissionController;
//# sourceMappingURL=rolePermission.controller.js.map
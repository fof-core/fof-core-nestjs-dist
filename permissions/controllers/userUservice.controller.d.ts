import { CrudController, CrudRequest } from "@nestjsx/crud";
import { UserUservice } from '../entities/userUservice.entity';
import { userUserviceService } from '../services/userUservice.service';
import { FofUservice } from '../../uservices/uservices.service';
import { UserService } from '../services/user.service';
export declare class userUserviceController implements CrudController<UserUservice> {
    service: userUserviceService;
    private fofUservice;
    private userService;
    constructor(service: userUserviceService, fofUservice: FofUservice, userService: UserService);
    private fofConfig;
    get base(): CrudController<UserUservice>;
    createOne(req: CrudRequest, dto: UserUservice): Promise<UserUservice>;
}

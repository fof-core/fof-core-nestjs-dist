import { CrudController, CrudRequest } from "@nestjsx/crud";
import { FofUser } from "../entities/user.entity";
import { UserRoleOrganization } from '../entities/userRoleOrganization.entity';
import { UserService } from "../services/user.service";
import { ifofSearch } from '../../core/entity/entity.interfaces';
import { InsertResult, DeleteResult } from "typeorm";
import { iUser } from '../../shared/user.interface';
import { FofUservice } from '../../uservices/uservices.service';
import { FofHardcodedOptions } from '../../core/core.interface';
export declare class UserController implements CrudController<FofUser> {
    service: UserService;
    private fofUservice;
    private config;
    constructor(service: UserService, fofUservice: FofUservice, config: FofHardcodedOptions);
    get base(): CrudController<FofUser>;
    private fofConfig;
    private isMasterForUserManagement;
    createOne(req: CrudRequest, dto: FofUser): Promise<FofUser>;
    updateOne(req: CrudRequest, dto: FofUser): Promise<FofUser>;
    deleteOne(req: CrudRequest, id: number): Promise<FofUser | void>;
    userRolesUpdate(userId: number, organisationId: number, userRoleOrganizations: UserRoleOrganization[]): Promise<InsertResult>;
    userRolesDelete(userId: number, organizationsId: string): Promise<DeleteResult>;
    getMany(user: iUser, limit?: number, page?: number, sort?: string, order?: string, organizationsId?: string, fullSearch?: string): Promise<ifofSearch<FofUser>>;
}

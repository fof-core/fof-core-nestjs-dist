"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const openapi = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
const crud_1 = require("@nestjsx/crud");
const userUservice_entity_1 = require("../entities/userUservice.entity");
const userUservice_service_1 = require("../services/userUservice.service");
const crud_decorators_1 = require("../../core/crud.decorators");
const uservices_service_1 = require("../../uservices/uservices.service");
const fof_configuration_1 = require("../../core/fof.configuration");
const user_service_1 = require("../services/user.service");
let userUserviceController = class userUserviceController {
    constructor(service, fofUservice, userService) {
        this.service = service;
        this.fofUservice = fofUservice;
        this.userService = userService;
        this.fofConfig = fof_configuration_1.fofConfig();
    }
    get base() {
        return this;
    }
    async createOne(req, dto) {
        const newUserUService = await this.base.createOneBase(req, dto);
        // if (newUserUService && newUserUService.id && this.fofConfig.rabbitMQ.url) {
        //   const userForNotification = this.userService.
        //   this.fofUservice.publishTopicMessage(
        //     newUser, efofServiceTopic.fofUsers, eFofServiceMethod.create)
        // }
        return newUserUService;
    }
};
__decorate([
    crud_1.Override(),
    openapi.ApiResponse({ status: 200, type: require("../entities/userUservice.entity").UserUservice }),
    __param(0, crud_1.ParsedRequest()),
    __param(1, crud_1.ParsedBody()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, userUservice_entity_1.UserUservice]),
    __metadata("design:returntype", Promise)
], userUserviceController.prototype, "createOne", null);
userUserviceController = __decorate([
    crud_decorators_1.FofCrud({
        entity: userUservice_entity_1.UserUservice
    }),
    crud_decorators_1.FofSwagger('userUservice'),
    common_1.Controller('userUservices'),
    __metadata("design:paramtypes", [userUservice_service_1.userUserviceService,
        uservices_service_1.FofUservice,
        user_service_1.UserService])
], userUserviceController);
exports.userUserviceController = userUserviceController;
//# sourceMappingURL=userUservice.controller.js.map
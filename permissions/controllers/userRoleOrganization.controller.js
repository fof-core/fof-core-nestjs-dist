"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const openapi = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
const userRoleOrganization_entity_1 = require("../entities/userRoleOrganization.entity");
const userRoleOrganization_service_1 = require("../services/userRoleOrganization.service");
const crud_decorators_1 = require("../../core/crud.decorators");
let UserRoleOrganizationController = class UserRoleOrganizationController {
    constructor(service) {
        this.service = service;
    }
};
UserRoleOrganizationController = __decorate([
    crud_decorators_1.FofCrud({
        entity: userRoleOrganization_entity_1.UserRoleOrganization
    }),
    crud_decorators_1.FofSwagger('UserRoleOrganizations'),
    common_1.Controller('userRoleOrganizations'),
    __metadata("design:paramtypes", [userRoleOrganization_service_1.UserRoleOrganizationService])
], UserRoleOrganizationController);
exports.UserRoleOrganizationController = UserRoleOrganizationController;
//# sourceMappingURL=userRoleOrganization.controller.js.map
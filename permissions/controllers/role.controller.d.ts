import { CrudController } from "@nestjsx/crud";
import { Role } from "../entities/role.entity";
import { RoleService } from "../services/role.service";
export declare class RoleController implements CrudController<Role> {
    service: RoleService;
    constructor(service: RoleService);
    test(): Promise<{
        ok: number;
    }>;
}

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const openapi = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
const crud_1 = require("@nestjsx/crud");
const user_entity_1 = require("../entities/user.entity");
const userRoleOrganization_entity_1 = require("../entities/userRoleOrganization.entity");
const user_service_1 = require("../services/user.service");
const permission_enum_1 = require("../../shared/permission.enum");
const crud_decorators_1 = require("../../core/crud.decorators");
const core_decorators_1 = require("../../core/core.decorators");
const swagger_1 = require("@nestjs/swagger");
const user_interface_1 = require("../../shared/user.interface");
const uservices_service_1 = require("../../uservices/uservices.service");
const uservices_interface_1 = require("../../uservices/uservices.interface");
const fof_configuration_1 = require("../../core/fof.configuration");
const core_interface_1 = require("../../core/core.interface");
let UserController = class UserController {
    constructor(service, fofUservice, config) {
        this.service = service;
        this.fofUservice = fofUservice;
        this.config = config;
        this.isMasterForUserManagement = false;
        this.fofConfig = fof_configuration_1.fofConfig();
        this.isMasterForUserManagement = config.isMasterForUserManagement;
    }
    get base() {
        return this;
    }
    // Additionnal
    // @FofRBACPermissions (
    //   eCp.roleCreate, 
    //   eCp.userCreate
    // )
    // Only if users rights are added
    async createOne(req, dto) {
        const newUser = await this.base.createOneBase(req, dto);
        // if (newUser && newUser.id && 
        //     this.isMasterForUserManagement && this.fofConfig.rabbitMQ.url) {
        //       this.service.userChangeNotification(newUser.id, eFofServiceMethod.create)
        //   this.fofUservice.publishTopicMessage(
        //     newUser, efofServiceTopic.fofUsers, eFofServiceMethod.create)
        // }
        if (newUser && newUser.id) {
            this.service.userChangeNotification(newUser.id, uservices_interface_1.eFofServiceMethod.delete);
        }
        return newUser;
    }
    async updateOne(req, dto) {
        const updatedUser = await this.base.updateOneBase(req, dto);
        if (updatedUser && updatedUser.id) {
            this.service.userChangeNotification(updatedUser.id, uservices_interface_1.eFofServiceMethod.update);
        }
        // if (updatedUser && updatedUser.id && 
        //   this.isMasterForUserManagement && this.fofConfig.rabbitMQ.url) {
        //     this.service.userChangeNotification(updatedUser.id, eFofServiceMethod.update)
        //   this.fofUservice.publishTopicMessage(
        //     updatedUser, efofServiceTopic.fofUsers, eFofServiceMethod.update)      
        // }
        return updatedUser;
    }
    async deleteOne(req, id) {
        const result = await this.base.deleteOneBase(req);
        // if (id && this.fofConfig.rabbitMQ.url && this.isMasterForUserManagement) {
        //   this.fofUservice.publishTopicMessage(
        //     id, efofServiceTopic.fofUsers, eFofServiceMethod.delete)
        // }
        if (id) {
            this.service.userChangeNotification(id, uservices_interface_1.eFofServiceMethod.delete);
        }
        return result;
    }
    async userRolesUpdate(userId, organisationId, userRoleOrganizations) {
        return await this.service.roleByUserAndOrganizationSave(userId, organisationId, userRoleOrganizations);
    }
    async userRolesDelete(userId, organizationsId) {
        const orgsId = JSON.parse(organizationsId);
        return await this.service.roleByUserAndOrganizationsDelete(userId, orgsId);
    }
    async getMany(user, limit, page, sort, order, organizationsId, fullSearch) {
        return await this.service.searchUsers({
            limit: limit,
            page: page,
            sort: sort,
            order: order,
            organizationsId: organizationsId,
            fullSearch: fullSearch
        }, user);
    }
};
__decorate([
    crud_1.Override(),
    openapi.ApiResponse({ status: 200, type: require("../entities/user.entity").FofUser }),
    __param(0, crud_1.ParsedRequest()),
    __param(1, crud_1.ParsedBody()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, user_entity_1.FofUser]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "createOne", null);
__decorate([
    crud_1.Override(),
    openapi.ApiResponse({ status: 200, type: require("../entities/user.entity").FofUser }),
    __param(0, crud_1.ParsedRequest()),
    __param(1, crud_1.ParsedBody()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, user_entity_1.FofUser]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "updateOne", null);
__decorate([
    crud_1.Override(),
    openapi.ApiResponse({ status: 200, type: Object }),
    __param(0, crud_1.ParsedRequest()),
    __param(1, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Number]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "deleteOne", null);
__decorate([
    swagger_1.ApiOperation({
        summary: 'Replace user roles for one organization',
        description: `Replace all the user roles for a given organization by the sent roles, 
                  even if they are not in the same organisation`
    }),
    swagger_1.ApiBody({ type: [userRoleOrganization_entity_1.UserRoleOrganization] }),
    common_1.Put(':userId/organization/:organisationId/roles'),
    openapi.ApiResponse({ status: 200 }),
    __param(0, common_1.Param('userId')),
    __param(1, common_1.Param('organisationId')),
    __param(2, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Number, Array]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "userRolesUpdate", null);
__decorate([
    swagger_1.ApiOperation({
        summary: 'Delete user roles for several organizations',
        description: `Array of organizationId: All user roles relative to that organization will be deleted`
    }),
    swagger_1.ApiQuery({ name: 'ids', description: 'Must be an array in json format' }),
    swagger_1.ApiBody({ type: [Number] }),
    common_1.Delete(':userId/organizations'),
    openapi.ApiResponse({ status: 200 }),
    __param(0, common_1.Param('userId')),
    __param(1, common_1.Query('ids')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, String]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "userRolesDelete", null);
__decorate([
    common_1.Get(),
    openapi.ApiResponse({ status: 200 }),
    __param(0, core_decorators_1.FofUserParam()),
    __param(1, common_1.Query('limit')),
    __param(2, common_1.Query('page')),
    __param(3, common_1.Query('sort')),
    __param(4, common_1.Query('order')),
    __param(5, common_1.Query('organizationsId')),
    __param(6, common_1.Query('fullSearch')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_interface_1.iUser, Number, Number, String, String, String, String]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "getMany", null);
UserController = __decorate([
    crud_decorators_1.FofCrud({
        entity: user_entity_1.FofUser,
        permissions: {
            create: [permission_enum_1.eCp.userCreate],
            delete: [permission_enum_1.eCp.userDelete],
            read: [permission_enum_1.eCp.userRead],
            update: [permission_enum_1.eCp.userUpdate],
        },
        excludeFields: ['password'],
        allowToJoin: [
            { name: 'userRoleOrganizations' },
            { name: 'userRoleOrganizations.role' },
            { name: 'userRoleOrganizations.organization' },
            { name: 'userUServices' }
        ],
        implementCustom: [
            crud_decorators_1.eImplementCustom.getMany
        ]
    }),
    crud_decorators_1.FofSwagger('Users'),
    common_1.Controller("users"),
    __param(2, common_1.Inject(core_interface_1.FOF_CORE_OPTIONS)),
    __metadata("design:paramtypes", [user_service_1.UserService,
        uservices_service_1.FofUservice, Object])
], UserController);
exports.UserController = UserController;
//# sourceMappingURL=user.controller.js.map
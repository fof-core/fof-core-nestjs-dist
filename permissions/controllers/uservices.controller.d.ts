import { FofuService } from '../entities/uservice.entity';
import { UserviceCrudService } from '../services/uservice.service';
import { CrudController, CrudRequest } from '@nestjsx/crud';
import { FofUservice } from '../../uservices/uservices.service';
import { FofHardcodedOptions } from '../../core/core.interface';
export declare class UservicesController implements CrudController<FofuService> {
    service: UserviceCrudService;
    private fofUservice;
    private config;
    constructor(service: UserviceCrudService, fofUservice: FofUservice, config: FofHardcodedOptions);
    get base(): CrudController<FofuService>;
    private fofConfig;
    private isMasterForMiniServicesManagement;
    createOne(req: CrudRequest, dto: FofuService): Promise<FofuService>;
    updateOne(req: CrudRequest, dto: FofuService): Promise<FofuService>;
    deleteOne(req: CrudRequest, id: number): Promise<FofuService | void>;
}

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const openapi = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
const crud_1 = require("@nestjsx/crud");
const organization_entity_1 = require("../entities/organization.entity");
const organization_service_1 = require("../services/organization.service");
const permission_enum_1 = require("../../shared/permission.enum");
const crud_decorators_1 = require("../../core/crud.decorators");
const swagger_1 = require("@nestjs/swagger");
const core_decorators_1 = require("../../core/core.decorators");
const user_interface_1 = require("../../shared/user.interface");
const permissions_decorator_1 = require("../permissions.decorator");
let OrganizationController = class OrganizationController {
    constructor(service) {
        this.service = service;
    }
    get base() {
        return this;
    }
    async getTReeview(user) {
        return this.service.getOrganizationTree(user);
    }
    async createOne(req, dto) {
        const result = await this.base.createOneBase(req, dto);
        this.service.OrganizationTreeFlatChange();
        return result;
    }
    async deleteOne(req) {
        const result = await this.base.deleteOneBase(req);
        this.service.OrganizationTreeFlatChange();
        return result;
    }
};
__decorate([
    swagger_1.ApiOperation({
        summary: 'Get all organizations in once',
        description: `Get all organizations in once, ordered in a treeview structure.`
    }),
    common_1.Get('getTreeView'),
    openapi.ApiResponse({ status: 200, type: [require("../entities/organization.entity").Organization] }),
    __param(0, core_decorators_1.FofUserParam()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_interface_1.iUser]),
    __metadata("design:returntype", Promise)
], OrganizationController.prototype, "getTReeview", null);
__decorate([
    permissions_decorator_1.FofRBACPermissions(permission_enum_1.eCp.organizationCreate),
    crud_1.Override(),
    openapi.ApiResponse({ status: 200, type: require("../entities/organization.entity").Organization }),
    __param(0, crud_1.ParsedRequest()),
    __param(1, crud_1.ParsedBody()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, organization_entity_1.Organization]),
    __metadata("design:returntype", Promise)
], OrganizationController.prototype, "createOne", null);
__decorate([
    permissions_decorator_1.FofRBACPermissions(permission_enum_1.eCp.organizationDelete),
    crud_1.Override(),
    openapi.ApiResponse({ status: 200, type: Object }),
    __param(0, crud_1.ParsedRequest()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], OrganizationController.prototype, "deleteOne", null);
OrganizationController = __decorate([
    crud_decorators_1.FofCrud({
        entity: organization_entity_1.Organization,
        permissions: {
            create: [permission_enum_1.eCp.organizationCreate],
            delete: [permission_enum_1.eCp.organizationDelete],
            read: [permission_enum_1.eCp.organizationRead],
            update: [permission_enum_1.eCp.organizationUpdate],
        }
    }),
    crud_decorators_1.FofSwagger('Organizations'),
    common_1.Controller('organizations'),
    __metadata("design:paramtypes", [organization_service_1.OrganizationService])
], OrganizationController);
exports.OrganizationController = OrganizationController;
//# sourceMappingURL=organization.controller.js.map
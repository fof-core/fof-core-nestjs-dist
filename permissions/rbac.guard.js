"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
let FofRBACGuard = class FofRBACGuard {
    constructor(reflector) {
        this.reflector = reflector;
    }
    async canActivate(context) {
        const permissionsToCheck = this.reflector.get('permissions', context.getHandler());
        const request = context.switchToHttp().getRequest();
        const user = request.user;
        if (!permissionsToCheck) {
            return true;
        }
        // console.log('user', user)
        const found = Object.values(user.permissions).some((r) => permissionsToCheck.indexOf(r) >= 0);
        if (found) {
            return true;
        }
        return false;
    }
};
FofRBACGuard = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [core_1.Reflector])
], FofRBACGuard);
exports.FofRBACGuard = FofRBACGuard;
//# sourceMappingURL=rbac.guard.js.map
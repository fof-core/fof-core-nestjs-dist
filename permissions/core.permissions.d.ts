export declare enum eP {
    userCreate = "userCreate",
    userUpdate = "userUpdate",
    userRead = "userRead",
    userDelete = "userDelete",
    roleCreate = "roleCreate",
    roleUpdate = "roleUpdate",
    roleRead = "roleRead",
    roleDelete = "roleDelete",
    userRoleOrganizationCreate = "userRoleOrganizationCreate",
    userRoleOrganizationUpdate = "userRoleOrganizationUpdate",
    userRoleOrganizationRead = "userRoleOrganizationRead",
    userRoleOrganizationDelete = "userRoleOrganizationDelete"
}

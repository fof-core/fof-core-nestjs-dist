import { FofuService } from '../entities/uservice.entity';
import { FofTypeOrmCrudService } from '../../core/typeorm-crud.service';
import { Repository } from 'typeorm';
import { FofHardcodedOptions } from '../../core/core.interface';
import { FofUservice } from '../../uservices/uservices.service';
export declare class UserviceCrudService extends FofTypeOrmCrudService<FofuService> {
    private FofuServiceRepo;
    private config;
    private fofUservice;
    constructor(FofuServiceRepo: Repository<FofuService>, config: FofHardcodedOptions, fofUservice: FofUservice);
    private isMasterForMiniServicesManagement;
    private uServiceUpdateOne;
    private uServiceCreateOne;
    private uServiceDeleteOne;
}

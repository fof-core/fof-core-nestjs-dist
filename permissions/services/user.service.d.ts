import { FofUser } from '../entities/user.entity';
import { UserRoleOrganization } from '../entities/userRoleOrganization.entity';
import { Repository } from 'typeorm';
import { CacheService } from '../../shared/cache.service';
import { ifofSearch } from '../../core/entity/entity.interfaces';
import { FofTypeOrmCrudService } from '../../core/typeorm-crud.service';
import { iUser } from '../../shared/user.interface';
import { FofHardcodedOptions } from '../../core/core.interface';
import { FofUservice } from '../../uservices/uservices.service';
import { eUsMessage, eFofServiceMethod } from '../../uservices/uservices.interface';
interface isearchUsers {
    organizationsId?: string;
    limit?: number;
    page?: number;
    sort?: string;
    order?: any;
    fullSearch?: string;
}
export declare class UserService extends FofTypeOrmCrudService<FofUser> {
    private userRepo;
    private config;
    private cacheService;
    private fofUservice;
    constructor(userRepo: Repository<FofUser>, config: FofHardcodedOptions, cacheService: CacheService, fofUservice: FofUservice);
    private isMasterForUserManagement;
    private fofConfig;
    uServiceMessageManage(msg: eUsMessage): Promise<void | import("@golevelup/nestjs-rabbitmq").Nack>;
    private userUpdateOne;
    private userCreateOne;
    private userDeleteOne;
    userChangeNotification(id: number, method: eFofServiceMethod): Promise<void>;
    roleByUserAndOrganizationSave(userId: number, organizationId: number, userRoleOrganizations: UserRoleOrganization[]): Promise<import("typeorm").InsertResult>;
    roleByUserAndOrganizationsDeleteSave(userId: number, organizationsId: number[]): Promise<import("typeorm").DeleteResult>;
    roleByUserAndOrganizationsDelete(userId: number, organizationsId: number[]): Promise<import("typeorm").DeleteResult>;
    searchUsers(search: isearchUsers, user: iUser): Promise<ifofSearch<FofUser>>;
}
export {};

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const uservice_entity_1 = require("../entities/uservice.entity");
const typeorm_crud_service_1 = require("../../core/typeorm-crud.service");
const core_constants_1 = require("../../core/core.constants");
const typeorm_2 = require("typeorm");
const core_interface_1 = require("../../core/core.interface");
const uservices_service_1 = require("../../uservices/uservices.service");
const fof_configuration_1 = require("../../core/fof.configuration");
let UserviceCrudService = class UserviceCrudService extends typeorm_crud_service_1.FofTypeOrmCrudService {
    constructor(FofuServiceRepo, config, fofUservice) {
        super(FofuServiceRepo);
        this.FofuServiceRepo = FofuServiceRepo;
        this.config = config;
        this.fofUservice = fofUservice;
        // @RabbitSubscribe({
        //   exchange: FOF_USERVICE_CHANNEL,
        //   routingKey: efofServiceTopic.fofUsers,
        //   queue: 'fof-app-test'
        // })
        // public async pubSubHandler(msg: {}) {
        //   console.log(`Received user`);
        // }
        // @RabbitSubscribe({
        //   exchange: FOF_USERVICE_CHANNEL,
        //   routingKey: efofServiceTopic.fofUservice,
        //   queue: 'fof-app-test'
        // })
        // public async pubSubHandler2(msg: {}) {
        //   console.log(`Received uservice: `);
        // }
        this.isMasterForMiniServicesManagement = false;
        this.isMasterForMiniServicesManagement = config.isMasterForMiniServicesManagement;
        const _fofConfig = fof_configuration_1.fofConfig();
        // if (_fofConfig.rabbitMQ.url && !this.isMasterForMiniServicesManagement) {      
        //   //toDo: manage user-organizations     
        //   this.fofUservice.subscribeTopicMessage([      
        //     efofServiceTopic.fofUservice
        //   ], async (msg:eUsMessage) => {
        //     console.log('uService sub', msg)
        //     if (!this.isMasterForMiniServicesManagement && msg.topic === efofServiceTopic.fofUservice ) {          
        //       switch (msg.method) {
        //         case eFofServiceMethod.update:
        //           return this.uServiceUpdateOne(msg.dto)            
        //         case eFofServiceMethod.create:
        //           return this.uServiceCreateOne(msg.dto)            
        //         case eFofServiceMethod.delete:
        //           //toDo: deactivate more than delete if no records associated with the user
        //           // try to delete, if constrint error -> deactivate
        //           return this.uServiceDeleteOne(msg.dto) 
        //         default:
        //           break;
        //       }        
        //     }       
        //   })
        // }
        // this.fofUservice.subscribeTopicMessage([      
        //   efofServiceTopic.fofUservice
        // ], async (msg:eUsMessage) => {
        //   console.log('uService sub', msg)        
        // })
        // this.fofUservice.subscribeTopicMessage([      
        //   efofServiceTopic.fofUsers
        // ], async (msg:eUsMessage) => {
        //   console.log('user sub', msg)        
        // })
    }
    async uServiceUpdateOne(uServiceToUpdate) {
        console.log(uServiceToUpdate);
        const update = await this.FofuServiceRepo.createQueryBuilder()
            .update()
            .set(Object.assign(Object.assign({}, uServiceToUpdate), { id: null }))
            .where("id = :id", { id: uServiceToUpdate.id })
            .execute();
        //for now, no way to verify if something is updated or not
        // if update = 0
        // return new fofNack()
    }
    async uServiceCreateOne(uServiceToCreate) {
        let newUservice = new uservice_entity_1.FofuService();
        this.FofuServiceRepo.merge(newUservice, uServiceToCreate);
        newUservice = await this.FofuServiceRepo.save(newUservice);
        if (!newUservice) {
            return new uservices_service_1.fofNack();
        }
    }
    async uServiceDeleteOne(id) {
        let result = await this.FofuServiceRepo.delete({ id: id });
        // no way to verify
        // if (!result) {
        //   return new fofNack()
        // }
    }
};
UserviceCrudService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(uservice_entity_1.FofuService, core_constants_1.FOF_DEFAULT_CONNECTION)),
    __param(1, common_1.Inject(core_interface_1.FOF_CORE_OPTIONS)),
    __metadata("design:paramtypes", [typeorm_2.Repository, Object, uservices_service_1.FofUservice])
], UserviceCrudService);
exports.UserviceCrudService = UserviceCrudService;
//# sourceMappingURL=uservice.service.js.map
import { UserUservice } from '../entities/userUservice.entity';
import { FofTypeOrmCrudService } from '../../core/typeorm-crud.service';
import { Repository } from 'typeorm';
export declare class userUserviceService extends FofTypeOrmCrudService<UserUservice> {
    constructor(repo: Repository<UserUservice>);
}

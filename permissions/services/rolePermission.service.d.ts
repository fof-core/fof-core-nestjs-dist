import { RolePermission } from '../entities/rolePermission.entitiy';
import { FofTypeOrmCrudService } from '../../core/typeorm-crud.service';
import { Repository } from 'typeorm';
export declare class RolePermissionService extends FofTypeOrmCrudService<RolePermission> {
    constructor(repo: Repository<RolePermission>);
}

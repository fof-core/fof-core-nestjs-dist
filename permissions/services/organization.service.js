"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const organization_entity_1 = require("../entities/organization.entity");
const entities_views_1 = require("../entities/entities.views");
const typeorm_3 = require("typeorm");
const typeorm_crud_service_1 = require("../../core/typeorm-crud.service");
const permission_enum_1 = require("../../shared/permission.enum");
const core_constants_1 = require("../../core/core.constants");
let OrganizationService = class OrganizationService extends typeorm_crud_service_1.FofTypeOrmCrudService {
    constructor(organizationRepo) {
        super(organizationRepo);
        this.organizationRepo = organizationRepo;
    }
    async getOrganizationTree(user) {
        // get the allowed organization for the user
        const userAllowedOrganizations = await typeorm_2.getRepository(entities_views_1.vOrganizationPermission, core_constants_1.FOF_DEFAULT_CONNECTION)
            .createQueryBuilder('vOrganizationPermission')
            .select('organizationChildId')
            .where('userId = :id', { id: user.id })
            .andWhere(`permissionCode = '${permission_enum_1.eCp.organizationRead}'`)
            .execute();
        // get the organization flat tree
        const fullFlatTree = await typeorm_2.getRepository(organization_entity_1.Organization, core_constants_1.FOF_DEFAULT_CONNECTION)
            .createQueryBuilder('Organization')
            .select('id')
            .addSelect('uid')
            .addSelect('name')
            .addSelect('parentId')
            .orderBy('id')
            .addOrderBy('parentId')
            .execute();
        // check all the ancestors for a given node
        const checkParent = (node) => {
            let currentNode;
            for (let index = 0; index < fullFlatTree.length; index++) {
                currentNode = fullFlatTree[index];
                if (currentNode.id === node.parentId) {
                    currentNode.mustKeep = true;
                    if (currentNode.parentId) {
                        checkParent(currentNode);
                    }
                    return;
                }
            }
        };
        // check node as must be keep
        for (let index = 0; index < fullFlatTree.length; index++) {
            const org = fullFlatTree[index];
            if (userAllowedOrganizations.filter(uao => uao.organizationChildId === org.id).length > 0) {
                org.mustKeep = true;
                if (org.parentId) {
                    checkParent(org);
                }
            }
        }
        const nest = (orgs, id = null) => orgs
            .filter(org => org.parentId === id)
            .map(org => (Object.assign(Object.assign({}, org), { children: nest(orgs, org.id) })));
        // build tree
        const tree = nest(fullFlatTree.filter(org => org.mustKeep));
        return tree;
    }
    async OrganizationTreeFlatChange() {
        const manager = this.organizationRepo.manager;
        const tree = await manager.getTreeRepository(organization_entity_1.Organization).findTrees();
        let flatTree = [];
        // iterate on the tree for flatening it with all deep children for each node
        const getSelectedNode = (node, rootNode) => {
            if (flatTree.filter(m => m.organizationParentId === node.id && m.organizationChildId === node.id).length === 0) {
                flatTree.push({
                    organizationParentId: node.id,
                    organizationChildId: node.id,
                    organizationParentUid: node.uid,
                    organizationChildUid: node.uid
                });
            }
            if (rootNode && flatTree.filter(m => m.organizationParentId === rootNode.id && m.organizationChildId === node.id).length === 0) {
                flatTree.push({
                    organizationParentId: rootNode.id,
                    organizationChildId: node.id,
                    organizationParentUid: rootNode.uid,
                    organizationChildUid: node.uid
                });
            }
            node.children.forEach(child => {
                if (rootNode) {
                    getSelectedNode(child, rootNode);
                }
                getSelectedNode(child, node);
            });
        };
        if (tree && tree.length > 0) {
            getSelectedNode(tree[0]);
        }
        flatTree = flatTree.sort((a, b) => {
            if (a.organizationParentId > b.organizationParentId)
                return 1;
            if (b.organizationParentId > a.organizationParentId)
                return -1;
        });
        return await typeorm_3.getConnection(core_constants_1.FOF_DEFAULT_CONNECTION).transaction(async (transactionalEntityManager) => {
            await transactionalEntityManager
                .createQueryBuilder()
                .delete()
                .from(organization_entity_1.OrganizationFlat)
                .execute();
            return await transactionalEntityManager
                .createQueryBuilder()
                .insert()
                .into(organization_entity_1.OrganizationFlat)
                .values(flatTree)
                .execute();
        });
    }
};
OrganizationService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(organization_entity_1.Organization, core_constants_1.FOF_DEFAULT_CONNECTION)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], OrganizationService);
exports.OrganizationService = OrganizationService;
//# sourceMappingURL=organization.service.js.map
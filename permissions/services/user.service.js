"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const user_entity_1 = require("../entities/user.entity");
const userRoleOrganization_entity_1 = require("../entities/userRoleOrganization.entity");
const typeorm_2 = require("typeorm");
const cache_service_1 = require("../../shared/cache.service");
const typeorm_crud_service_1 = require("../../core/typeorm-crud.service");
const core_interface_1 = require("../../core/core.interface");
const uservices_service_1 = require("../../uservices/uservices.service");
const uservices_interface_1 = require("../../uservices/uservices.interface");
const fof_configuration_1 = require("../../core/fof.configuration");
const core_constants_1 = require("../../core/core.constants");
let UserService = class UserService extends typeorm_crud_service_1.FofTypeOrmCrudService {
    constructor(userRepo, config, cacheService, fofUservice) {
        super(userRepo);
        this.userRepo = userRepo;
        this.config = config;
        this.cacheService = cacheService;
        this.fofUservice = fofUservice;
        this.isMasterForUserManagement = false;
        this.isMasterForUserManagement = config.isMasterForUserManagement;
        this.fofConfig = fof_configuration_1.fofConfig();
    }
    async uServiceMessageManage(msg) {
        const user = msg.dto;
        let userIsRegisteredForService = false;
        // is the user registered for that service?
        if (user.userUServices) {
            user.userUServices.forEach(uus => {
                if (uus.uService) {
                    if (uus.uService.technicalName.toLowerCase() ===
                        this.fofConfig.service.technicalName.toLowerCase()) {
                        userIsRegisteredForService = true;
                    }
                }
            });
        }
        if (!userIsRegisteredForService)
            return;
        switch (msg.method) {
            case uservices_interface_1.eFofServiceMethod.update:
                return this.userUpdateOne(msg.dto);
            case uservices_interface_1.eFofServiceMethod.create:
                return this.userCreateOne(msg.dto);
            case uservices_interface_1.eFofServiceMethod.delete:
                //toDo: deactivate more than delete if no records associated with the user
                // try to delete, if constrint error -> deactivate
                return this.userDeleteOne(msg.dto);
            default:
                break;
        }
    }
    async userUpdateOne(userToUpdate) {
        // toDo : verify version before saving?
        const update = await this.userRepo.createQueryBuilder()
            .update()
            // .set(userToUpdate)
            .set({
            firstName: userToUpdate.firstName,
            lastName: userToUpdate.lastName,
            login: userToUpdate.login,
            email: userToUpdate.email,
            organizationId: userToUpdate.organizationId
        })
            .where("id = :id", { id: userToUpdate.id })
            .execute();
        //for now, no way to verify if something is updated or not
        // if update = 0
        // return new fofNack()
    }
    async userCreateOne(userToUpdate) {
        let newUser = new user_entity_1.FofUser();
        this.userRepo.merge(newUser, userToUpdate);
        newUser = await this.userRepo.save(newUser);
        if (!newUser) {
            return new uservices_service_1.fofNack();
        }
    }
    async userDeleteOne(id) {
        let result = await this.userRepo.delete({ id: id });
        console.log('deleted servcie', result);
        // if (!newUser) {
        //   return new fofNack()
        // }
    }
    // a terminer !!
    async userChangeNotification(id, method) {
        if (this.isMasterForUserManagement && this.fofConfig.rabbitMQ.url) {
            const userNotif = await typeorm_2.getRepository(user_entity_1.FofUser, core_constants_1.FOF_DEFAULT_CONNECTION)
                .createQueryBuilder('user')
                .select([
                'user.id',
                'user.email',
                'user.login',
                'user.firstName',
                'user.lastName',
                'userUService.uServiceId',
                'userUService._updatedDate',
                'uService.technicalName'
            ])
                .leftJoin('user.userUServices', 'userUService')
                .leftJoin('userUService.uService', 'uService')
                .where('user.id = :id', { id: id })
                .getOne();
            this.fofUservice.publishTopicMessage(userNotif, uservices_interface_1.efofServiceTopic.fofUsers, method);
            // console.log('userNotif', userNotif)
            // console.log(userNotif.email)
        }
    }
    async roleByUserAndOrganizationSave(userId, organizationId, userRoleOrganizations) {
        return await typeorm_2.getConnection(core_constants_1.FOF_DEFAULT_CONNECTION).transaction(async (transactionalEntityManager) => {
            this.cacheService.del(`user_${userId}`);
            await transactionalEntityManager
                .createQueryBuilder()
                .delete()
                .from(userRoleOrganization_entity_1.UserRoleOrganization)
                .where('userId = :id', { id: userId })
                .andWhere('organizationId = :organizationId', { organizationId: organizationId })
                .execute();
            return await transactionalEntityManager
                .createQueryBuilder()
                .insert()
                .into(userRoleOrganization_entity_1.UserRoleOrganization)
                .values(userRoleOrganizations)
                .execute();
        });
    }
    async roleByUserAndOrganizationsDeleteSave(userId, organizationsId) {
        this.cacheService.del(`user_${userId}`);
        return await typeorm_2.getManager(core_constants_1.FOF_DEFAULT_CONNECTION)
            .createQueryBuilder()
            .delete()
            .from(userRoleOrganization_entity_1.UserRoleOrganization)
            .where('userId = :id', { id: userId })
            .andWhere('organizationId IN (:...organizationsId)', { organizationsId: organizationsId })
            .execute();
    }
    async roleByUserAndOrganizationsDelete(userId, organizationsId) {
        this.cacheService.del(`user_${userId}`);
        return await typeorm_2.getManager(core_constants_1.FOF_DEFAULT_CONNECTION)
            .createQueryBuilder()
            .delete()
            .from(userRoleOrganization_entity_1.UserRoleOrganization)
            .where('userId = :id', { id: userId })
            .andWhere('organizationId IN (:...organizationsId)', { organizationsId: organizationsId })
            .execute();
    }
    async searchUsers(search, user) {
        let userSearch = typeorm_2.getRepository(user_entity_1.FofUser, core_constants_1.FOF_DEFAULT_CONNECTION)
            .createQueryBuilder('user')
            .innerJoin('user.organization', 'organization')
            // access management
            .innerJoin('vOrganizationPermission', 'op', 'user.organizationId = op.organizationChildId');
        if (search.organizationsId) {
            userSearch = userSearch
                .innerJoin('OrganizationFlat', 'oflat', 'organization.id = oflat.organizationChildId');
        }
        userSearch = userSearch
            .where('op.userId = :userId', { userId: user.id })
            .andWhere("op.permissionCode = 'userRead'");
        if (search.organizationsId) {
            userSearch = userSearch
                .andWhere('oflat.organizationParentId = :organizationParentId', { organizationParentId: search.organizationsId });
        }
        if (search.fullSearch) {
            userSearch = userSearch
                .andWhere(new typeorm_2.Brackets(qb => {
                qb.where("user.email like :email", { email: `%${search.fullSearch}%` })
                    .orWhere("user.firstName like :firstName", { firstName: `%${search.fullSearch}%` })
                    .orWhere("user.lastName like :lastName", { lastName: `%${search.fullSearch}%` })
                    .orWhere("user.login like :login", { login: `%${search.fullSearch}%` });
            }));
        }
        const [data, count] = await userSearch
            .limit(search.limit || 10000)
            .offset((search.page || 0) * search.limit)
            .orderBy(`user.${search.sort || 'email'}`, search.order.toUpperCase() || 'ASC')
            .getManyAndCount();
        return {
            count: 1,
            page: 1,
            pageCount: 5,
            total: count,
            data: data
        };
    }
};
UserService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(user_entity_1.FofUser, core_constants_1.FOF_DEFAULT_CONNECTION)),
    __param(1, common_1.Inject(core_interface_1.FOF_CORE_OPTIONS)),
    __metadata("design:paramtypes", [typeorm_2.Repository, Object, cache_service_1.CacheService,
        uservices_service_1.FofUservice])
], UserService);
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map
import { Permission } from '../entities/permission.entity';
import { FofTypeOrmCrudService } from '../../core/typeorm-crud.service';
import { Repository } from 'typeorm';
export declare class PermissionService extends FofTypeOrmCrudService<Permission> {
    constructor(repo: Repository<Permission>);
}

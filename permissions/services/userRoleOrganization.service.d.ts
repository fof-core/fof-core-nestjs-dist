import { UserRoleOrganization } from '../entities/userRoleOrganization.entity';
import { FofTypeOrmCrudService } from '../../core/typeorm-crud.service';
import { Repository } from 'typeorm';
export declare class UserRoleOrganizationService extends FofTypeOrmCrudService<UserRoleOrganization> {
    constructor(repo: Repository<UserRoleOrganization>);
}

import { Role } from '../entities/role.entity';
import { FofTypeOrmCrudService } from '../../core/typeorm-crud.service';
import { Repository } from 'typeorm';
export declare class RoleService extends FofTypeOrmCrudService<Role> {
    constructor(repo: Repository<Role>);
}

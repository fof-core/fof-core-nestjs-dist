import { Repository } from 'typeorm';
import { Organization } from '../entities/organization.entity';
import { FofTypeOrmCrudService } from '../../core/typeorm-crud.service';
import { iUser } from '../../shared/user.interface';
export declare class OrganizationService extends FofTypeOrmCrudService<Organization> {
    private organizationRepo;
    constructor(organizationRepo: Repository<Organization>);
    getOrganizationTree(user: iUser): Promise<any>;
    OrganizationTreeFlatChange(): Promise<import("typeorm").InsertResult>;
}

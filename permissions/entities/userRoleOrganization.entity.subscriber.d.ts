import { EntitySubscriberInterface, InsertEvent, Connection, RemoveEvent } from 'typeorm';
import { UserRoleOrganization } from './userRoleOrganization.entity';
import { CacheService } from '../../shared/cache.service';
export declare class UserRoleOrganizationSubscriber implements EntitySubscriberInterface<UserRoleOrganization> {
    readonly connection: Connection;
    private cacheService;
    constructor(connection: Connection, cacheService: CacheService);
    listenTo(): typeof UserRoleOrganization;
    afterInsert(event: InsertEvent<UserRoleOrganization>): void;
    beforeRemove(event: RemoveEvent<UserRoleOrganization>): void;
}

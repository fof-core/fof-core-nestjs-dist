export declare class vOrganizationPermission {
    organizationParentId: number;
    organizationChildId: number;
    organizationParentUid: number;
    organizationChildUid: number;
    userId: number;
    permissionCode: string;
}

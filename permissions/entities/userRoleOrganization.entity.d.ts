import { EntityCore } from '../../core/entity/core.entity';
import { Role } from './role.entity';
import { FofUser } from './user.entity';
import { Organization } from './organization.entity';
export declare class UserRoleOrganization extends EntityCore {
    userId: number;
    roleId: number;
    organizationId: number;
    order: number;
    user: FofUser;
    role: Role;
    organization: Organization;
}

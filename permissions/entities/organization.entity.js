"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const openapi = require("@nestjs/swagger");
const typeorm_1 = require("typeorm");
const swagger_1 = require("@nestjs/swagger");
const core_entity_1 = require("../../core/entity/core.entity");
const userRoleOrganization_entity_1 = require("./userRoleOrganization.entity");
const user_entity_1 = require("./user.entity");
let Organization = class Organization extends core_entity_1.EntityCore {
    static _OPENAPI_METADATA_FACTORY() {
        return { uid: { required: true, type: () => Number }, name: { required: true, type: () => String }, description: { required: false, type: () => String }, children: { required: false, type: () => [require("./organization.entity").Organization] }, parentId: { required: false, type: () => Number }, parent: { required: false, type: () => require("./organization.entity").Organization } };
    }
};
__decorate([
    swagger_1.ApiProperty()
    // @Column({ type: 'nvarchar', length: 100, nullable: true})
    ,
    typeorm_1.Index('idx_uid'),
    typeorm_1.Column(),
    __metadata("design:type", Number)
], Organization.prototype, "uid", void 0);
__decorate([
    swagger_1.ApiProperty(),
    typeorm_1.Column({ type: 'nvarchar', length: 100 }),
    __metadata("design:type", String)
], Organization.prototype, "name", void 0);
__decorate([
    typeorm_1.Column({ type: 'nvarchar', length: 200, nullable: true }),
    __metadata("design:type", String)
], Organization.prototype, "description", void 0);
__decorate([
    typeorm_1.TreeChildren(),
    __metadata("design:type", Array)
], Organization.prototype, "children", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", Number)
], Organization.prototype, "parentId", void 0);
__decorate([
    typeorm_1.TreeParent(),
    __metadata("design:type", Organization)
], Organization.prototype, "parent", void 0);
__decorate([
    swagger_1.ApiHideProperty(),
    typeorm_1.OneToMany(type => userRoleOrganization_entity_1.UserRoleOrganization, userRoleOrganization => userRoleOrganization.role),
    __metadata("design:type", Array)
], Organization.prototype, "userRoleOrganizations", void 0);
__decorate([
    swagger_1.ApiHideProperty(),
    typeorm_1.OneToMany(type => user_entity_1.FofUser, user => user.organization),
    __metadata("design:type", Array)
], Organization.prototype, "users", void 0);
__decorate([
    swagger_1.ApiHideProperty(),
    typeorm_1.OneToMany(type => OrganizationFlat, organizationFlat => organizationFlat.organization),
    typeorm_1.JoinColumn({ name: 'id' }),
    __metadata("design:type", Array)
], Organization.prototype, "organizationFlats", void 0);
Organization = __decorate([
    typeorm_1.Entity('fof_organization'),
    typeorm_1.Tree('nested-set')
], Organization);
exports.Organization = Organization;
let OrganizationFlat = class OrganizationFlat {
    static _OPENAPI_METADATA_FACTORY() {
        return { organizationParentId: { required: true, type: () => Object }, organizationChildId: { required: true, type: () => Object }, organizationParentUid: { required: true, type: () => Object }, organizationChildUid: { required: true, type: () => Object }, organization: { required: false, type: () => require("./organization.entity").Organization } };
    }
};
__decorate([
    typeorm_1.PrimaryColumn(),
    __metadata("design:type", Number)
], OrganizationFlat.prototype, "organizationParentId", void 0);
__decorate([
    typeorm_1.PrimaryColumn(),
    typeorm_1.Index('idx_organizationChildId'),
    __metadata("design:type", Number)
], OrganizationFlat.prototype, "organizationChildId", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", Number)
], OrganizationFlat.prototype, "organizationParentUid", void 0);
__decorate([
    typeorm_1.Index('idx_organizationChildUid'),
    typeorm_1.Column(),
    __metadata("design:type", Number)
], OrganizationFlat.prototype, "organizationChildUid", void 0);
__decorate([
    typeorm_1.ManyToOne(type => Organization, organization => organization.organizationFlats),
    typeorm_1.JoinColumn({ name: 'organizationChildId' }),
    __metadata("design:type", Organization)
], OrganizationFlat.prototype, "organization", void 0);
OrganizationFlat = __decorate([
    typeorm_1.Entity('fof_organization_flat')
], OrganizationFlat);
exports.OrganizationFlat = OrganizationFlat;
//# sourceMappingURL=organization.entity.js.map
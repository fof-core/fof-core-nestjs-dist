"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const permission_entity_1 = require("./permission.entity");
const rolePermission_entitiy_1 = require("./rolePermission.entitiy");
const role_entity_1 = require("./role.entity");
const user_entity_1 = require("./user.entity");
const userRoleOrganization_entity_1 = require("./userRoleOrganization.entity");
const organization_entity_1 = require("./organization.entity");
const entities_views_1 = require("./entities.views");
const uservice_entity_1 = require("./uservice.entity");
const userUservice_entity_1 = require("./userUservice.entity");
exports.permissionsEntities = [
    permission_entity_1.Permission, rolePermission_entitiy_1.RolePermission, role_entity_1.Role, user_entity_1.FofUser, userRoleOrganization_entity_1.UserRoleOrganization,
    organization_entity_1.Organization, organization_entity_1.OrganizationFlat, entities_views_1.vOrganizationPermission, uservice_entity_1.FofuService, userUservice_entity_1.UserUservice
];
//# sourceMappingURL=index.js.map
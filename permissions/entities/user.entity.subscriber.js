"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const typeorm_2 = require("@nestjs/typeorm");
const user_entity_1 = require("./user.entity");
const cache_service_1 = require("../../shared/cache.service");
const core_constants_1 = require("../../core/core.constants");
let UserSubscriber = class UserSubscriber {
    constructor(connection, cacheService) {
        this.connection = connection;
        this.cacheService = cacheService;
        connection.subscribers.push(this);
    }
    listenTo() {
        return user_entity_1.FofUser;
    }
    afterUpdate(event) {
        //ToDo: useless when update is made with a createQueryBuilder it seems...
        if (event.entity) {
            // console.log('update event')
            this.cacheService.del(`user_${event.entity.id}`);
        }
    }
};
UserSubscriber = __decorate([
    typeorm_1.EventSubscriber(),
    __param(0, typeorm_2.InjectConnection(core_constants_1.FOF_DEFAULT_CONNECTION)),
    __metadata("design:paramtypes", [typeorm_1.Connection,
        cache_service_1.CacheService])
], UserSubscriber);
exports.UserSubscriber = UserSubscriber;
//# sourceMappingURL=user.entity.subscriber.js.map
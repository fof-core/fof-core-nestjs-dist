import { EntityCore } from '../../core/entity/core.entity';
import { UserRoleOrganization } from './userRoleOrganization.entity';
import { RolePermission } from './rolePermission.entitiy';
export declare class Role extends EntityCore {
    code: string;
    description: string;
    userRoleOrganizations?: UserRoleOrganization[];
    rolePermissions?: RolePermission[];
}

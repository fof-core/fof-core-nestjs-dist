import { EntityCore } from '../../core/entity/core.entity';
import { FofuService } from './uservice.entity';
import { FofUser } from './user.entity';
export declare class UserUservice extends EntityCore {
    userId: number;
    uServiceId: number;
    user: FofUser;
    uService: FofuService;
}

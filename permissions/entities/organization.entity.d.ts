import { EntityCore } from '../../core/entity/core.entity';
import { UserRoleOrganization } from './userRoleOrganization.entity';
import { FofUser } from './user.entity';
export declare class Organization extends EntityCore {
    uid: number;
    name: string;
    description?: string;
    children?: Organization[];
    parentId?: number;
    parent?: Organization;
    userRoleOrganizations?: UserRoleOrganization[];
    users?: FofUser[];
    organizationFlats?: OrganizationFlat[];
}
export declare class OrganizationFlat {
    organizationParentId: Number;
    organizationChildId: Number;
    organizationParentUid: Number;
    organizationChildUid: Number;
    organization?: Organization;
}

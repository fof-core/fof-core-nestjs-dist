import { EntityCore } from '../../core/entity/core.entity';
import { Organization } from './organization.entity';
import { UserRoleOrganization } from './userRoleOrganization.entity';
import { eCp } from '../../shared/permission.enum';
import { UserUservice } from './userUservice.entity';
export declare class FofUser extends EntityCore {
    permissionInsert: eCp;
    email: string;
    login?: string;
    password: string;
    firstName?: string;
    lastName?: string;
    organizationId?: number;
    userRoleOrganizations?: UserRoleOrganization[];
    userUServices?: UserUservice[];
    organization?: Organization;
    hashPassword(): Promise<void>;
    passwordValidate(attempt: string): Promise<boolean>;
}

import { Permission } from './permission.entity';
import { RolePermission } from './rolePermission.entitiy';
import { Role } from './role.entity';
import { FofUser } from './user.entity';
import { UserRoleOrganization } from './userRoleOrganization.entity';
import { Organization, OrganizationFlat } from './organization.entity';
import { vOrganizationPermission } from './entities.views';
import { FofuService } from './uservice.entity';
import { UserUservice } from './userUservice.entity';
export declare const permissionsEntities: (typeof Organization | typeof UserRoleOrganization | typeof FofUser | typeof OrganizationFlat | typeof Role | typeof Permission | typeof RolePermission | typeof UserUservice | typeof FofuService | typeof vOrganizationPermission)[];

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const openapi = require("@nestjs/swagger");
const typeorm_1 = require("typeorm");
const swagger_1 = require("@nestjs/swagger");
const core_entity_1 = require("../../core/entity/core.entity");
const role_entity_1 = require("./role.entity");
const user_entity_1 = require("./user.entity");
const organization_entity_1 = require("./organization.entity");
let UserRoleOrganization = class UserRoleOrganization extends core_entity_1.EntityCore {
    static _OPENAPI_METADATA_FACTORY() {
        return { userId: { required: true, type: () => Number }, roleId: { required: true, type: () => Number }, organizationId: { required: true, type: () => Number }, order: { required: true, type: () => Number } };
    }
};
__decorate([
    swagger_1.ApiProperty()
    // @PrimaryColumn()
    ,
    typeorm_1.Column(),
    __metadata("design:type", Number)
], UserRoleOrganization.prototype, "userId", void 0);
__decorate([
    swagger_1.ApiProperty()
    // @PrimaryColumn()
    ,
    typeorm_1.Column(),
    __metadata("design:type", Number)
], UserRoleOrganization.prototype, "roleId", void 0);
__decorate([
    swagger_1.ApiProperty()
    // @PrimaryColumn()
    ,
    typeorm_1.Column(),
    __metadata("design:type", Number)
], UserRoleOrganization.prototype, "organizationId", void 0);
__decorate([
    swagger_1.ApiProperty(),
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", Number)
], UserRoleOrganization.prototype, "order", void 0);
__decorate([
    swagger_1.ApiHideProperty(),
    typeorm_1.ManyToOne(type => user_entity_1.FofUser, user => user.userRoleOrganizations),
    __metadata("design:type", user_entity_1.FofUser
    // @ApiProperty({type:() => Role})
    )
], UserRoleOrganization.prototype, "user", void 0);
__decorate([
    swagger_1.ApiHideProperty(),
    typeorm_1.ManyToOne(type => role_entity_1.Role, role => role.userRoleOrganizations),
    __metadata("design:type", role_entity_1.Role
    // @ApiProperty({type:() => Organization})
    )
], UserRoleOrganization.prototype, "role", void 0);
__decorate([
    swagger_1.ApiHideProperty(),
    typeorm_1.ManyToOne(type => organization_entity_1.Organization, organization => organization.userRoleOrganizations),
    __metadata("design:type", organization_entity_1.Organization)
], UserRoleOrganization.prototype, "organization", void 0);
UserRoleOrganization = __decorate([
    typeorm_1.Entity('fof_user_role_organization'),
    typeorm_1.Unique('user_role_organization_uq_idx', ['userId', 'roleId', 'organizationId'])
], UserRoleOrganization);
exports.UserRoleOrganization = UserRoleOrganization;
//# sourceMappingURL=userRoleOrganization.entity.js.map
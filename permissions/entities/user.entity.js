"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const openapi = require("@nestjs/swagger");
const typeorm_1 = require("typeorm");
const swagger_1 = require("@nestjs/swagger");
const core_entity_1 = require("../../core/entity/core.entity");
const organization_entity_1 = require("./organization.entity");
const userRoleOrganization_entity_1 = require("./userRoleOrganization.entity");
const bcrypt_1 = require("bcrypt");
const permission_enum_1 = require("../../shared/permission.enum");
const userUservice_entity_1 = require("./userUservice.entity");
let FofUser = 
// Only for MySQL
// @Index('idx_user_fulltext', ['email', 'login', 'firstName', 'lastName'], { fulltext: true })
class FofUser extends core_entity_1.EntityCore {
    constructor() {
        super(...arguments);
        this.permissionInsert = permission_enum_1.eCp.userCreate;
    }
    async hashPassword() {
        /** Password could be null when windows auth is used */
        if (!this.password) {
            return;
        }
        this.password = await bcrypt_1.hash(this.password, 10);
    }
    async passwordValidate(attempt) {
        if (!attempt) {
            return true;
        }
        return await bcrypt_1.compare(attempt, this.password);
    }
    static _OPENAPI_METADATA_FACTORY() {
        return { email: { required: true, type: () => String }, login: { required: false, type: () => String }, password: { required: true, type: () => String }, firstName: { required: false, type: () => String }, lastName: { required: false, type: () => String }, organizationId: { required: false, type: () => Number } };
    }
};
__decorate([
    swagger_1.ApiHideProperty(),
    __metadata("design:type", Object)
], FofUser.prototype, "permissionInsert", void 0);
__decorate([
    swagger_1.ApiProperty(),
    typeorm_1.Index('idx_email'),
    typeorm_1.Column({ type: 'nvarchar', length: 60 }),
    __metadata("design:type", String)
], FofUser.prototype, "email", void 0);
__decorate([
    swagger_1.ApiProperty(),
    typeorm_1.Index('idx_login'),
    typeorm_1.Column({ nullable: true, length: 30 }),
    __metadata("design:type", String)
], FofUser.prototype, "login", void 0);
__decorate([
    swagger_1.ApiProperty(),
    typeorm_1.Column({ select: false, type: 'nvarchar', length: 100, nullable: true }),
    __metadata("design:type", String)
], FofUser.prototype, "password", void 0);
__decorate([
    swagger_1.ApiProperty(),
    typeorm_1.Index('idx_firstname'),
    typeorm_1.Column({ nullable: true, length: 30 }),
    __metadata("design:type", String)
], FofUser.prototype, "firstName", void 0);
__decorate([
    swagger_1.ApiProperty(),
    typeorm_1.Index('idx_lastname'),
    typeorm_1.Column({ nullable: true, length: 30 }),
    __metadata("design:type", String)
], FofUser.prototype, "lastName", void 0);
__decorate([
    swagger_1.ApiProperty(),
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", Number)
], FofUser.prototype, "organizationId", void 0);
__decorate([
    swagger_1.ApiHideProperty(),
    typeorm_1.OneToMany(() => userRoleOrganization_entity_1.UserRoleOrganization, userRoleOrganization => userRoleOrganization.user, { cascade: true }),
    __metadata("design:type", Array)
], FofUser.prototype, "userRoleOrganizations", void 0);
__decorate([
    swagger_1.ApiHideProperty(),
    typeorm_1.OneToMany(() => userUservice_entity_1.UserUservice, userUservice => userUservice.user, { cascade: true }),
    __metadata("design:type", Array)
], FofUser.prototype, "userUServices", void 0);
__decorate([
    swagger_1.ApiHideProperty(),
    typeorm_1.ManyToOne(type => organization_entity_1.Organization, organization => organization.users),
    __metadata("design:type", organization_entity_1.Organization)
], FofUser.prototype, "organization", void 0);
__decorate([
    typeorm_1.BeforeInsert(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], FofUser.prototype, "hashPassword", null);
FofUser = __decorate([
    typeorm_1.Entity('fof_user')
    // Only for MySQL
    // @Index('idx_user_fulltext', ['email', 'login', 'firstName', 'lastName'], { fulltext: true })
], FofUser);
exports.FofUser = FofUser;
//# sourceMappingURL=user.entity.js.map
import { EntityCore } from '../../core/entity/core.entity';
export declare class FofuService extends EntityCore {
    technicalName: string;
    name?: string;
    frontUrl?: string;
    backUrl?: string;
    availableForUsers?: boolean;
    userUServices?: FofuService[];
}

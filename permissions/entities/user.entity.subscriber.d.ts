import { EntitySubscriberInterface, UpdateEvent, Connection } from 'typeorm';
import { FofUser } from './user.entity';
import { CacheService } from '../../shared/cache.service';
export declare class UserSubscriber implements EntitySubscriberInterface<FofUser> {
    readonly connection: Connection;
    private cacheService;
    constructor(connection: Connection, cacheService: CacheService);
    listenTo(): typeof FofUser;
    afterUpdate(event: UpdateEvent<FofUser>): void;
}

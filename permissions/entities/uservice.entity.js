"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const openapi = require("@nestjs/swagger");
const typeorm_1 = require("typeorm");
const swagger_1 = require("@nestjs/swagger");
const core_entity_1 = require("../../core/entity/core.entity");
// import { FofUser } from '../../permissions/entities/user.entity'
const userUservice_entity_1 = require("./userUservice.entity");
let FofuService = class FofuService extends core_entity_1.EntityCore {
    static _OPENAPI_METADATA_FACTORY() {
        return { technicalName: { required: true, type: () => String }, name: { required: false, type: () => String }, frontUrl: { required: false, type: () => String }, backUrl: { required: false, type: () => String }, availableForUsers: { required: false, type: () => Boolean } };
    }
};
__decorate([
    swagger_1.ApiProperty(),
    typeorm_1.Column({ length: 30 }),
    __metadata("design:type", String)
], FofuService.prototype, "technicalName", void 0);
__decorate([
    swagger_1.ApiProperty(),
    typeorm_1.Column({ length: 80, nullable: true }),
    __metadata("design:type", String)
], FofuService.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty(),
    typeorm_1.Column({ length: 200, nullable: true }),
    __metadata("design:type", String)
], FofuService.prototype, "frontUrl", void 0);
__decorate([
    swagger_1.ApiProperty(),
    typeorm_1.Column({ length: 200, nullable: true }),
    __metadata("design:type", String)
], FofuService.prototype, "backUrl", void 0);
__decorate([
    swagger_1.ApiProperty(),
    typeorm_1.Column(),
    __metadata("design:type", Boolean)
], FofuService.prototype, "availableForUsers", void 0);
__decorate([
    swagger_1.ApiHideProperty(),
    typeorm_1.OneToMany(() => userUservice_entity_1.UserUservice, userUservice => userUservice.uService, { cascade: true }),
    __metadata("design:type", Array)
], FofuService.prototype, "userUServices", void 0);
FofuService = __decorate([
    typeorm_1.Entity('fof_mini_service')
], FofuService);
exports.FofuService = FofuService;
//# sourceMappingURL=uservice.entity.js.map
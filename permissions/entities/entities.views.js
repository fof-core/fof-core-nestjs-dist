"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
let vOrganizationPermission = class vOrganizationPermission {
};
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", Number)
], vOrganizationPermission.prototype, "organizationParentId", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", Number)
], vOrganizationPermission.prototype, "organizationChildId", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", Number)
], vOrganizationPermission.prototype, "organizationParentUid", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", Number)
], vOrganizationPermission.prototype, "organizationChildUid", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", Number)
], vOrganizationPermission.prototype, "userId", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], vOrganizationPermission.prototype, "permissionCode", void 0);
vOrganizationPermission = __decorate([
    typeorm_1.ViewEntity('fof_v_organization_permission', {
        expression: (connection) => connection.createQueryBuilder()
            .select('o.id', 'organizationParentId')
            .addSelect('oflat.organizationChildId', 'organizationChildId')
            .addSelect('o.uid', 'organizationParentUid')
            .addSelect('oflat.organizationChildUid', 'organizationChildUid')
            .addSelect('us.id', 'userId')
            .addSelect('p.code', 'permissionCode')
            .from('fof_user', 'us')
            .innerJoin('fof_user_role_organization', 'uro', 'us.id = uro.userId')
            .innerJoin('fof_role', 'r', 'uro.roleId = r.id')
            .innerJoin('fof_role_permission', 'rp', 'r.id = rp.roleId')
            .innerJoin('fof_permission', 'p', 'rp.permissionId = p.id')
            .innerJoin('fof_organization', 'o', 'uro.organizationId = o.id')
            .innerJoin('fof_organization_flat', 'oflat', 'o.id = oflat.organizationParentId')
            .groupBy('o.id')
            .addGroupBy('oflat.organizationChildId')
            .addGroupBy('us.id')
            .addGroupBy('p.code')
            .addGroupBy('o.uid')
            .addGroupBy('oflat.organizationChildUid')
    })
], vOrganizationPermission);
exports.vOrganizationPermission = vOrganizationPermission;
//# sourceMappingURL=entities.views.js.map
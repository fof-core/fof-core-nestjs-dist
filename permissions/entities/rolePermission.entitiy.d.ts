import { EntityCore } from '../../core/entity/core.entity';
import { Role } from './role.entity';
import { Permission } from './permission.entity';
export declare class RolePermission extends EntityCore {
    permissionId: number;
    roleId: number;
    order: number;
    permission: Permission;
    role: Role;
}

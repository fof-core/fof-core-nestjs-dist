"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const swagger_1 = require("@nestjs/swagger");
const core_entity_1 = require("../../core/entity/core.entity");
const role_entity_1 = require("./role.entity");
const permission_entity_1 = require("./permission.entity");
let RolePermission = class RolePermission extends core_entity_1.EntityCore {
};
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", Number)
], RolePermission.prototype, "permissionId", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", Number)
], RolePermission.prototype, "roleId", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", Number)
], RolePermission.prototype, "order", void 0);
__decorate([
    swagger_1.ApiHideProperty(),
    typeorm_1.ManyToOne(type => permission_entity_1.Permission, permission => permission.rolePermissions),
    __metadata("design:type", permission_entity_1.Permission)
], RolePermission.prototype, "permission", void 0);
__decorate([
    swagger_1.ApiHideProperty(),
    typeorm_1.ManyToOne(type => role_entity_1.Role, role => role.rolePermissions, { onDelete: 'CASCADE' }),
    __metadata("design:type", role_entity_1.Role)
], RolePermission.prototype, "role", void 0);
RolePermission = __decorate([
    typeorm_1.Entity('fof_role_permission'),
    typeorm_1.Index('role_permission_uq_idx', ['roleId', 'permissionId'], { unique: true })
], RolePermission);
exports.RolePermission = RolePermission;
//# sourceMappingURL=rolePermission.entitiy.js.map
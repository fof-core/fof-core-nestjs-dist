import { EntityCore } from '../../core/entity/core.entity';
import { RolePermission } from './rolePermission.entitiy';
export declare class Permission extends EntityCore {
    code: string;
    description: string;
    rolePermissions: RolePermission[];
}

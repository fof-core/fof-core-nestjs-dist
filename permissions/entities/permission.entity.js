"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const openapi = require("@nestjs/swagger");
const typeorm_1 = require("typeorm");
const swagger_1 = require("@nestjs/swagger");
const core_entity_1 = require("../../core/entity/core.entity");
const rolePermission_entitiy_1 = require("./rolePermission.entitiy");
let Permission = class Permission extends core_entity_1.EntityCore {
    static _OPENAPI_METADATA_FACTORY() {
        return { code: { required: true, type: () => String }, description: { required: true, type: () => String } };
    }
};
__decorate([
    swagger_1.ApiProperty(),
    typeorm_1.Column({ type: 'nvarchar', length: 100 }),
    __metadata("design:type", String)
], Permission.prototype, "code", void 0);
__decorate([
    swagger_1.ApiProperty(),
    typeorm_1.Column({ type: 'nvarchar', length: 100, nullable: true }),
    __metadata("design:type", String)
], Permission.prototype, "description", void 0);
__decorate([
    swagger_1.ApiHideProperty(),
    typeorm_1.OneToMany(type => rolePermission_entitiy_1.RolePermission, rolePermission => rolePermission.role, { cascade: true, onDelete: 'CASCADE' }),
    __metadata("design:type", Array)
], Permission.prototype, "rolePermissions", void 0);
Permission = __decorate([
    typeorm_1.Entity('fof_permission')
], Permission);
exports.Permission = Permission;
//# sourceMappingURL=permission.entity.js.map
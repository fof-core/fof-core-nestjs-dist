"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const openapi = require("@nestjs/swagger");
const typeorm_1 = require("typeorm");
const swagger_1 = require("@nestjs/swagger");
const core_entity_1 = require("../../core/entity/core.entity");
// import { Role } from './role.entity'
const uservice_entity_1 = require("./uservice.entity");
const user_entity_1 = require("./user.entity");
let UserUservice = class UserUservice extends core_entity_1.EntityCore {
    static _OPENAPI_METADATA_FACTORY() {
        return { userId: { required: true, type: () => Number }, uServiceId: { required: true, type: () => Number } };
    }
};
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", Number)
], UserUservice.prototype, "userId", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", Number)
], UserUservice.prototype, "uServiceId", void 0);
__decorate([
    swagger_1.ApiHideProperty(),
    typeorm_1.ManyToOne(type => user_entity_1.FofUser, user => user.userUServices),
    __metadata("design:type", user_entity_1.FofUser
    // @ApiProperty({type:() => Role})
    )
], UserUservice.prototype, "user", void 0);
__decorate([
    swagger_1.ApiHideProperty(),
    typeorm_1.ManyToOne(type => uservice_entity_1.FofuService, uService => uService.userUServices),
    __metadata("design:type", uservice_entity_1.FofuService)
], UserUservice.prototype, "uService", void 0);
UserUservice = __decorate([
    typeorm_1.Entity('fof_user_mini_service'),
    typeorm_1.Unique('user_mini_service_uq_idx', ['userId', 'uServiceId'])
], UserUservice);
exports.UserUservice = UserUservice;
//# sourceMappingURL=userUservice.entity.js.map
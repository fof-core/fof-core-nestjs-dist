import { Repository } from 'typeorm';
import { FofUser } from '../permissions/entities/user.entity';
import { Role } from '../permissions/entities/role.entity';
import { Permission } from '../permissions/entities/permission.entity';
import { Organization } from '../permissions/entities/organization.entity';
import { CurrentService } from '../core/current.service';
import { FofHardcodedOptions } from '../core/core.interface';
import { OrganizationService } from '../permissions/services/organization.service';
import { FofuService } from '../permissions/entities/uservice.entity';
export declare class InitService {
    private readonly organizationRepository;
    private readonly userRepository;
    private readonly roleRepository;
    private readonly permissionRepository;
    private readonly rolePermissionRepository;
    private readonly UserRoleOrganizationRepository;
    private readonly FofuServiceRepo;
    private config;
    private organizationService;
    private currentService;
    constructor(organizationRepository: Repository<Organization>, userRepository: Repository<FofUser>, roleRepository: Repository<Role>, permissionRepository: Repository<Permission>, rolePermissionRepository: Repository<Permission>, UserRoleOrganizationRepository: Repository<Permission>, FofuServiceRepo: Repository<FofuService>, config: FofHardcodedOptions, organizationService: OrganizationService, currentService: CurrentService);
    private defaultUservcies;
    private defaultUsers;
    private defaultRoles;
    private defaultOrganizations;
    private defaultDataCreate;
}

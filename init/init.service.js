"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("typeorm");
const typeorm_2 = require("@nestjs/typeorm");
const user_entity_1 = require("../permissions/entities/user.entity");
const role_entity_1 = require("../permissions/entities/role.entity");
const permission_entity_1 = require("../permissions/entities/permission.entity");
const userRoleOrganization_entity_1 = require("../permissions/entities/userRoleOrganization.entity");
const organization_entity_1 = require("../permissions/entities/organization.entity");
const rolePermission_entitiy_1 = require("../permissions/entities/rolePermission.entitiy");
const current_service_1 = require("../core/current.service");
const core_interface_1 = require("../core/core.interface");
const permission_enum_1 = require("../shared/permission.enum");
const organization_service_1 = require("../permissions/services/organization.service");
const uservice_entity_1 = require("../permissions/entities/uservice.entity");
const core_constants_1 = require("../core/core.constants");
// const organizationsTest: any = require('./organisations.json')
// const organizationsTest: Organization[] = require('./realOrganisation.json')
let InitService = class InitService {
    constructor(organizationRepository, userRepository, roleRepository, permissionRepository, rolePermissionRepository, UserRoleOrganizationRepository, FofuServiceRepo, config, organizationService, currentService) {
        this.organizationRepository = organizationRepository;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.permissionRepository = permissionRepository;
        this.rolePermissionRepository = rolePermissionRepository;
        this.UserRoleOrganizationRepository = UserRoleOrganizationRepository;
        this.FofuServiceRepo = FofuServiceRepo;
        this.config = config;
        this.organizationService = organizationService;
        this.currentService = currentService;
        // private defaultUservcies: FofuService[] = []
        this.defaultUservcies = [
            { technicalName: 'fof-nestjs-winauth', availableForUsers: false },
            { technicalName: 'fof-nestjs-logs', availableForUsers: false },
            { technicalName: 'fof-nestjs-uservice-management', availableForUsers: true, name: 'Gestion des mini-services' },
            { technicalName: 'fof-nestjs-users', availableForUsers: true, frontUrl: 'http://localhost:3000', name: 'Gestion des utilisateurs' },
            { technicalName: 'fof-app-test', availableForUsers: true, frontUrl: 'http://localhost:3001', name: 'Service de test' }
        ];
        this.defaultUsers = [
            { email: 'superuser@fof.com', password: 'changeMe', firstName: 'Super', lastName: 'User', organizationCode: '1' },
            { email: 'testuser@fof.com', password: 'changeMe', firstName: 'Test', lastName: 'User', organizationCode: '1' },
            { email: 'usertest2@fof.com', password: 'changeMe', firstName: 'Test2', lastName: 'User', organizationCode: '1' },
            { email: 'ffourteau@mycompany.ch', firstName: 'Fred', lastName: 'Fourteau', login: 'ffourteau', organizationCode: '1' }
        ];
        this.defaultRoles = [
            { code: 'superUser' },
            { code: 'userManager' },
            { code: 'organizationManager' },
            { code: 'roleManager' },
            { code: 'roleTest' }
        ];
        this.defaultOrganizations = [
            { id: 1, uid: 1, name: 'Organisation entière' },
            { id: 2, uid: 2, name: 'Organisation de test', parentId: 1 },
            { id: 3, uid: 3, name: 'Departement 1', parentId: 2 },
            { id: 4, uid: 4, name: 'Departement 2', parentId: 2 },
            { id: 5, uid: 5, name: 'Secteur 1-1', parentId: 3 },
            { id: 6, uid: 6, name: 'Secteur 1-2', parentId: 3 },
            { id: 7, uid: 7, name: 'Secteur 2-1', parentId: 4 }
        ];
        this.currentService.setPermissions(config.permissions);
        // if (!config.dontMountDatabase) {
        this.defaultDataCreate();
        // }    
    }
    async defaultDataCreate() {
        //ToDO: how for the first?
        // this.currentService.user = {
        //   id: 1, firstName: 'System', lastName: 'Bot'
        // }
        let organizationAdded = false;
        //add organisation 
        for (let index = 0; index < this.defaultOrganizations.length; index++) {
            const orgItem = this.defaultOrganizations[index];
            const orgToSave = new organization_entity_1.Organization();
            this.organizationRepository.merge(orgToSave, orgItem);
            await this.organizationRepository.save(orgToSave);
            organizationAdded = true;
        }
        // add default services
        for (let index = 0; index < this.defaultUservcies.length; index++) {
            const service = this.defaultUservcies[index];
            let wService = await this.FofuServiceRepo.findOne({ where: { technicalName: service.technicalName } });
            if (!wService) {
                wService = new uservice_entity_1.FofuService();
                this.FofuServiceRepo.merge(wService, service);
                wService = await this.FofuServiceRepo.save(wService);
            }
        }
        // add default users   
        for (let index = 0; index < this.defaultUsers.length; index++) {
            const user = this.defaultUsers[index];
            let wUser = await this.userRepository.findOne({ where: { email: user.email } });
            if (!wUser) {
                wUser = new user_entity_1.FofUser();
                this.userRepository.merge(wUser, user);
                wUser = await this.userRepository.save(wUser);
                if (user.organizationCode) {
                    const organization = await this.organizationRepository.findOne({ where: { code: user.organizationCode } });
                    wUser.organizationId = organization.id;
                    wUser = await this.userRepository.save(wUser);
                }
            }
        }
        // Add default roles
        for (let index = 0; index < this.defaultRoles.length; index++) {
            const role = this.defaultRoles[index];
            let wRole = await this.roleRepository.findOne({ where: { code: role.code } });
            if (!wRole) {
                wRole = new role_entity_1.Role();
                this.roleRepository.merge(wRole, role);
                wRole = await this.roleRepository.save(wRole);
            }
        }
        // Add permissions
        const permissions = this.currentService.permissions;
        const permissionArray = Object.values(permissions);
        for (let index = 0; index < permissionArray.length; index++) {
            const permission = permissionArray[index];
            let wPermission = await this.permissionRepository.findOne({ where: { code: permission } });
            if (!wPermission) {
                wPermission = new permission_entity_1.Permission();
                wPermission.code = permission;
                wPermission = await this.permissionRepository.save(wPermission);
            }
        }
        // add permission to role function 
        const addPermisionToRole = async (permission, role) => {
            let wPermission = await this.permissionRepository.findOne({ where: { code: permission } });
            let wRole = await this.roleRepository.findOne({ where: { code: role } });
            let wRolePermission = await this.rolePermissionRepository.findOne({ where: { permissionId: wPermission.id, roleId: wRole.id } });
            if (!wRolePermission) {
                wRolePermission = new rolePermission_entitiy_1.RolePermission();
                wRolePermission.permission = wPermission;
                wRolePermission.role = wRole;
                wRolePermission = await this.rolePermissionRepository.save(wRolePermission);
            }
        };
        // roleManager
        await addPermisionToRole(permission_enum_1.eCp.roleCreate, 'roleManager');
        await addPermisionToRole(permission_enum_1.eCp.roleDelete, 'roleManager');
        await addPermisionToRole(permission_enum_1.eCp.roleRead, 'roleManager');
        await addPermisionToRole(permission_enum_1.eCp.roleUpdate, 'roleManager');
        // userManager
        await addPermisionToRole(permission_enum_1.eCp.userCreate, 'userManager');
        await addPermisionToRole(permission_enum_1.eCp.userDelete, 'userManager');
        await addPermisionToRole(permission_enum_1.eCp.userRead, 'userManager');
        await addPermisionToRole(permission_enum_1.eCp.userUpdate, 'userManager');
        await addPermisionToRole(permission_enum_1.eCp.userRoleCreate, 'userManager');
        await addPermisionToRole(permission_enum_1.eCp.userRoleDelete, 'userManager');
        await addPermisionToRole(permission_enum_1.eCp.userRoleUpdate, 'userManager');
        await addPermisionToRole(permission_enum_1.eCp.userRoleRead, 'userManager');
        // superUser -> all permissions
        for (let index = 0; index < permissionArray.length; index++) {
            const permission = permissionArray[index];
            await addPermisionToRole(permission, 'superUser');
        }
        //add role to user
        const addRolesToUser = async (role, userEmail, orgizationCode) => {
            let wRole = await this.roleRepository.findOne({ where: { code: role } });
            let wUser = await this.userRepository.findOne({ where: { email: userEmail } });
            let wOrganization;
            if (orgizationCode) {
                wOrganization = await this.organizationRepository.findOne({ where: { code: orgizationCode } });
            }
            let wUserRoleOrganization = await this.UserRoleOrganizationRepository.findOne({ where: { roleId: wRole.id, userId: wUser.id } });
            if (!wUserRoleOrganization) {
                wUserRoleOrganization = new userRoleOrganization_entity_1.UserRoleOrganization();
                wUserRoleOrganization.role = wRole;
                wUserRoleOrganization.user = wUser;
                if (orgizationCode) {
                    wUserRoleOrganization.organization = wOrganization;
                }
                wUserRoleOrganization = await this.UserRoleOrganizationRepository.save(wUserRoleOrganization);
            }
        };
        // await addRolesToUser('userManager','usertest@fof.com', 'dep1')
        // await addRolesToUser('roleTest','testuser@fof.com', 'dep1')
        await addRolesToUser('superUser', 'superuser@fof.com', '1');
        if (organizationAdded) {
            this.organizationService.OrganizationTreeFlatChange();
        }
    }
};
InitService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_2.InjectRepository(organization_entity_1.Organization, core_constants_1.FOF_DEFAULT_CONNECTION)),
    __param(1, typeorm_2.InjectRepository(user_entity_1.FofUser, core_constants_1.FOF_DEFAULT_CONNECTION)),
    __param(2, typeorm_2.InjectRepository(role_entity_1.Role, core_constants_1.FOF_DEFAULT_CONNECTION)),
    __param(3, typeorm_2.InjectRepository(permission_entity_1.Permission, core_constants_1.FOF_DEFAULT_CONNECTION)),
    __param(4, typeorm_2.InjectRepository(rolePermission_entitiy_1.RolePermission, core_constants_1.FOF_DEFAULT_CONNECTION)),
    __param(5, typeorm_2.InjectRepository(userRoleOrganization_entity_1.UserRoleOrganization, core_constants_1.FOF_DEFAULT_CONNECTION)),
    __param(6, typeorm_2.InjectRepository(uservice_entity_1.FofuService, core_constants_1.FOF_DEFAULT_CONNECTION)),
    __param(7, common_1.Inject(core_interface_1.FOF_CORE_OPTIONS)),
    __metadata("design:paramtypes", [typeorm_1.Repository,
        typeorm_1.Repository,
        typeorm_1.Repository,
        typeorm_1.Repository,
        typeorm_1.Repository,
        typeorm_1.Repository,
        typeorm_1.Repository, Object, organization_service_1.OrganizationService,
        current_service_1.CurrentService])
], InitService);
exports.InitService = InitService;
//# sourceMappingURL=init.service.js.map
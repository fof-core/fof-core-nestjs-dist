"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var FofCoreModule_1;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const core_module_1 = require("./core/core.module");
const uservices_module_1 = require("./uservices/uservices.module");
let FofCoreModule = FofCoreModule_1 = class FofCoreModule {
    // ToDo: async version
    static forRoot(options) {
        return {
            module: FofCoreModule_1,
            imports: [core_module_1.CoreModule.forRoot(options)],
            exports: [core_module_1.CoreModule]
        };
    }
};
FofCoreModule = FofCoreModule_1 = __decorate([
    common_1.Module({
        imports: [uservices_module_1.UservicesModule]
    })
], FofCoreModule);
exports.FofCoreModule = FofCoreModule;
//# sourceMappingURL=core.module.js.map
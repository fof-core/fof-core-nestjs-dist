import * as winston from 'winston';
import { FofHardcodedOptions } from './core.interface';
declare const logger: winston.Logger;
export declare class LoggerService {
    init(config: FofHardcodedOptions): void;
}
export { logger };

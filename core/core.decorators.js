"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
exports.FofUserParam = common_1.createParamDecorator(
//toDo: don't get an ExecutionContext in debbug mode
(data, ctx) => {
    try {
        const request = ctx.switchToHttp().getRequest();
        return request.user;
    }
    catch (error) {
        return ctx.user;
    }
});
//# sourceMappingURL=core.decorators.js.map
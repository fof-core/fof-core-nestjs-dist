import { TypeOrmModuleOptions } from '@nestjs/typeorm';
/** Authentication type.
 * Daefult is basic
 */
export declare enum eAuth {
    basic = "basic",
    windows = "windows"
}
export interface FofHardcodedOptions {
    /** By default, the service loads a .env file. You can choose to have
     * additional env files. the config musn't need to have the .env. but the file does
     * @example
     * additionalEndFilePaths: ['my-file']`, file name: .env.my-file
    */
    additionalEndFilePaths?: string[];
    /** a string enum for your action RBAC used in roles */
    permissions?: any;
    /** configuration for typeORM.
     * @see https://typeorm.io/
    */
    typeOrmModuleOptions?: TypeOrmModuleOptions;
    /** specify which service is the master for user mangement. For each type,
     *  only one service could exist in the same time in fof uService domain
     * */
    isMasterForUserManagement?: boolean;
    /** specify which service is the master for user mangement. For each type,
     *  only one service could exist in the same time in fof uService domain
     * */
    isMasterForOrganizationManagement?: boolean;
    /** specify which service is the master for mini service mangement. For each type,
     *  only one service could exist in the same time in fof mini service domain
     * */
    isMasterForMiniServicesManagement?: boolean;
    /** Do not set and configure a default database with user management
     *  By default, a SQLlite database is created in the /data folder
    */
    dontMountDatabase?: boolean;
}
/** for internal use only */
export declare const FOF_CORE_OPTIONS = "FofHardcodedOptions";
/** Implemented in core search. Should be implemented in all search with pagination */
export declare abstract class iSearchResult<T> {
    abstract count?: number;
    abstract total?: number;
    abstract page?: number;
    abstract pageCount?: number;
    abstract data: T[];
}

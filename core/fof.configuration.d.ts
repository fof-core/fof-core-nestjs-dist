export interface iFofCOnfig {
    service: {
        name: string;
        technicalName: string;
    };
    secret: string;
    cookie?: {
        path: string;
    };
    authentication?: {
        type: string;
        ldapConnexion: {
            url: string;
            base: string;
            bindDN: string;
            bindCredentials: string;
        };
    };
    jwt?: {
        expiresIn: string;
    };
    cacheOptions?: {
        /** seconds */
        ttl: number;
        /** maximum number of items in cache  */
        max: number;
    };
    debug?: {
        level?: string;
        consoleDebug?: boolean;
    };
    rabbitMQ?: {
        url: string;
        queue: string;
    };
    typeORM?: {
        dropAndSynchronize?: boolean;
    };
}
export declare const fofConfig: () => iFofCOnfig;

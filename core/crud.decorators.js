"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const crud_1 = require("@nestjsx/crud");
const auth_guard_1 = require("../auth/auth.guard");
const rbac_guard_1 = require("../permissions/rbac.guard");
const swagger_1 = require("@nestjs/swagger");
const permissions_decorator_1 = require("../permissions/permissions.decorator");
var eImplementCustom;
(function (eImplementCustom) {
    eImplementCustom["getOne"] = "getOne";
    eImplementCustom["getMany"] = "getMany";
    eImplementCustom["createOne"] = "createOne";
    eImplementCustom["createMany"] = "createMany";
    eImplementCustom["updateOne"] = "updateOne";
    eImplementCustom["replaceOne"] = "replaceOne";
    eImplementCustom["deleteOne"] = "deleteOne";
})(eImplementCustom = exports.eImplementCustom || (exports.eImplementCustom = {}));
/** Add several decorators and guards in once and set them
 *  @CrudAuth @see https://github.com/nestjsx/crud/wiki/Controllers
 *  @Crud @see https://github.com/nestjsx/crud/wiki/Controllers
 *  @UseGuards FofAuthJwtGuard & FofRBACGuard
 *  @param crudOptions @see https://github.com/nestjsx/crud/wiki/Controllers#options
 */
exports.FofCrud = (fofCrudOptions) => {
    const authOptions = {
        property: "user",
        persist: (user) => ({
            authUserId: user.id,
            authUserName: `${user.lastName} ${user.firstName}`
        })
    };
    let _crudOptions = {
        model: {
            type: ''
        },
        routes: {
            exclude: [],
            getManyBase: { decorators: [] },
            getOneBase: { decorators: [] },
            createOneBase: { decorators: [] },
            createManyBase: { decorators: [] },
            updateOneBase: { decorators: [] },
            replaceOneBase: { decorators: [] },
            deleteOneBase: { decorators: [] },
        },
        query: {
            join: {}
        }
    };
    if (!fofCrudOptions) {
        throw new Error('You must define the options with, at least, the entity type');
    }
    _crudOptions.model.type = fofCrudOptions.entity;
    if (fofCrudOptions.allowToJoin && fofCrudOptions.allowToJoin.length > 0) {
        fofCrudOptions.allowToJoin.forEach(join => {
            _crudOptions.query.join[join.name] = {};
        });
    }
    if (fofCrudOptions.implementCustom && fofCrudOptions.implementCustom.length > 0) {
        fofCrudOptions.implementCustom.forEach(custom => {
            _crudOptions.routes.exclude.push(`${custom}Base`);
        });
    }
    if (fofCrudOptions.excludeFields && fofCrudOptions.excludeFields.length > 0) {
        _crudOptions.query.exclude = fofCrudOptions.excludeFields;
    }
    if (fofCrudOptions.permissions) {
        if (fofCrudOptions.permissions.create) {
            _crudOptions.routes.createManyBase.decorators.push(permissions_decorator_1.FofRBACPermissions(...fofCrudOptions.permissions.create));
            _crudOptions.routes.createOneBase.decorators.push(permissions_decorator_1.FofRBACPermissions(...fofCrudOptions.permissions.create));
        }
        if (fofCrudOptions.permissions.update) {
            _crudOptions.routes.updateOneBase.decorators.push(permissions_decorator_1.FofRBACPermissions(...fofCrudOptions.permissions.update));
            _crudOptions.routes.replaceOneBase.decorators.push(permissions_decorator_1.FofRBACPermissions(...fofCrudOptions.permissions.update));
        }
        if (fofCrudOptions.permissions.delete) {
            _crudOptions.routes.deleteOneBase.decorators.push(permissions_decorator_1.FofRBACPermissions(...fofCrudOptions.permissions.delete));
        }
        if (fofCrudOptions.permissions.read) {
            _crudOptions.routes.getOneBase.decorators.push(permissions_decorator_1.FofRBACPermissions(...fofCrudOptions.permissions.read));
            _crudOptions.routes.getManyBase.decorators.push(permissions_decorator_1.FofRBACPermissions(...fofCrudOptions.permissions.read));
        }
    }
    Object.assign(_crudOptions, fofCrudOptions.crudOptions);
    return common_1.applyDecorators(
    // const apiName:string = crudOptions.model.type.constructor.name
    crud_1.CrudAuth(authOptions), 
    // order matter: crud must be after CrudAuth
    crud_1.Crud(_crudOptions), common_1.UseGuards(auth_guard_1.FofAuthJwtGuard, rbac_guard_1.FofRBACGuard));
};
/**
 * Add several Swagger Api decorator in once
 * @ApiBearerAuth
 * @ApiTags
 * @ApiResponse default ones
 */
exports.FofSwagger = (...tags) => {
    return common_1.applyDecorators(swagger_1.ApiBearerAuth(), swagger_1.ApiTags(...tags), swagger_1.ApiResponse({ status: 400, description: 'Bad request' }), swagger_1.ApiResponse({ status: 401, description: 'unauthorized.' }), swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }));
};
//# sourceMappingURL=crud.decorators.js.map
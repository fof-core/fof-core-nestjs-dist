"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const logger_service_1 = require("./logger.service");
const typeorm_1 = require("typeorm");
var eFofErrorMessage;
(function (eFofErrorMessage) {
    eFofErrorMessage["ERROR_NOT_MANAGED"] = "ERROR_NOT_MANAGED";
    eFofErrorMessage["FORBIDDEN"] = "FORBIDDEN";
})(eFofErrorMessage || (eFofErrorMessage = {}));
let AllExceptionsFilter = class AllExceptionsFilter {
    catch(exception, host) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse();
        const request = ctx.getRequest();
        let status = common_1.HttpStatus.INTERNAL_SERVER_ERROR;
        let instanceFounded = false;
        let logError = true;
        let errorManaged = false;
        let ok = false;
        let user;
        let message;
        let additionnalInformations;
        message = eFofErrorMessage.ERROR_NOT_MANAGED;
        if (!instanceFounded && exception instanceof typeorm_1.QueryFailedError) {
            instanceFounded = true;
            errorManaged = true;
            status = common_1.HttpStatus.BAD_REQUEST;
            message = exception.message;
        }
        if (!instanceFounded && exception instanceof common_1.HttpException) {
            errorManaged = true;
            instanceFounded = true;
            status = exception.getStatus();
            message = exception.message;
            if ([401, 403].includes(status)) {
                //we dont want to log 401 from now. Should be a parameter
                logError = false;
                message = exception.message.message;
            }
        }
        if (!instanceFounded) {
            errorManaged = false;
            if (exception.message) {
                additionnalInformations = exception.message;
            }
        }
        if (request.user) {
            user = {
                id: request.user.id,
                email: request.user.email
            };
        }
        const fofException = {
            errorManaged: errorManaged,
            fofCoreException: true,
            ok: ok,
            status: status,
            timestamp: new Date().toISOString(),
            url: request.url,
            user: user,
            message: message,
            error: additionnalInformations
        };
        if (logError) {
            logger_service_1.logger.error('fofExceptionFilter', fofException);
        }
        response.status(status).json(fofException);
    }
};
AllExceptionsFilter = __decorate([
    common_1.Catch()
], AllExceptionsFilter);
exports.AllExceptionsFilter = AllExceptionsFilter;
//# sourceMappingURL=all-exceptions.filter.js.map
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const permission_enum_1 = require("../shared/permission.enum");
let CurrentService = class CurrentService {
    setPermissions(permission) {
        this._permissions = Object.assign(Object.assign({}, permission), permission_enum_1.eCp);
    }
    get permissions() {
        return this._permissions;
    }
};
CurrentService = __decorate([
    common_1.Injectable()
], CurrentService);
exports.CurrentService = CurrentService;
//# sourceMappingURL=current.service.js.map
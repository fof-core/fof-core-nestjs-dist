import { eCp } from '../shared/permission.enum';
export declare class CurrentService {
    private _permissions;
    setPermissions(permission: any): void;
    get permissions(): eCp;
}

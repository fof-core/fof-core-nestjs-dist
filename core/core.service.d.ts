import { FofHardcodedOptions } from './core.interface';
import { LoggerService } from './logger.service';
import { FofUservice } from '../uservices/uservices.service';
import { UserService } from '../permissions/services/user.service';
export declare class CoreService {
    private hConfig;
    private readonly loggerService;
    private fofUservice;
    private userService;
    constructor(hConfig: FofHardcodedOptions, loggerService: LoggerService, fofUservice: FofUservice, userService: UserService);
    private fofConfig;
    private config;
    private manageReceivedMessage;
}

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const typeorm_2 = require("@nestjs/typeorm");
const current_service_1 = require("../current.service");
const core_constants_1 = require("../core.constants");
let AllEntitiesSubscriber = class AllEntitiesSubscriber {
    constructor(connection, currentService) {
        this.connection = connection;
        this.currentService = currentService;
        // How to inject service into typeorm subscriber
        // https://github.com/nestjs/typeorm/pull/27
        connection.subscribers.push(this);
    }
    /**
     * Called before all entities insertion.
     */
    beforeInsert(event) {
        // const entity = event.entity
        // if (entity) {
        //   if (entity.permissionInsert) {
        //     //toDO: check the permission view with userId and organizationId to verify if he's allowed to insert
        //     // console.log(entity.permissionInsert)
        //   }        
        //   event.entity._createdByName = `${this.currentService.user.lastName} ${this.currentService.user.firstName}`         
        //   event.entity._createdById = this.currentService.user.id
        // }
    }
    beforeUpdate(event) {
        // if (event.entity) {
        //   event.entity._updatedByName = `${this.currentService.user.lastName} ${this.currentService.user.firstName}`
        //   event.entity._updatedById = this.currentService.user.id
        // }
    }
};
AllEntitiesSubscriber = __decorate([
    typeorm_1.EventSubscriber(),
    __param(0, typeorm_2.InjectConnection(core_constants_1.FOF_DEFAULT_CONNECTION)),
    __metadata("design:paramtypes", [typeorm_1.Connection,
        current_service_1.CurrentService])
], AllEntitiesSubscriber);
exports.AllEntitiesSubscriber = AllEntitiesSubscriber;
//# sourceMappingURL=entity.subscriber.js.map
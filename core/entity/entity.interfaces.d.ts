export declare abstract class ifofSearch<T> {
    count: number;
    total: number;
    page: number;
    pageCount: number;
    data: T[];
}

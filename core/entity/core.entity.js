"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const openapi = require("@nestjs/swagger");
const typeorm_1 = require("typeorm");
const swagger_1 = require("@nestjs/swagger");
// export const columnsToExclud = ['_createdDate', '_createdById', '_createdByName']
class EntityCoreWithoutId {
    constructor() {
        this.permissionInsert = undefined;
        this.organizationId = undefined;
    }
}
__decorate([
    swagger_1.ApiHideProperty(),
    __metadata("design:type", String)
], EntityCoreWithoutId.prototype, "permissionInsert", void 0);
__decorate([
    swagger_1.ApiHideProperty(),
    __metadata("design:type", Number)
], EntityCoreWithoutId.prototype, "organizationId", void 0);
__decorate([
    swagger_1.ApiHideProperty(),
    typeorm_1.CreateDateColumn(),
    __metadata("design:type", Date)
], EntityCoreWithoutId.prototype, "_createdDate", void 0);
__decorate([
    swagger_1.ApiHideProperty(),
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", Number)
], EntityCoreWithoutId.prototype, "_createdById", void 0);
__decorate([
    swagger_1.ApiHideProperty(),
    typeorm_1.Column({ nullable: true, length: 40 }),
    __metadata("design:type", String)
], EntityCoreWithoutId.prototype, "_createdByName", void 0);
__decorate([
    swagger_1.ApiHideProperty(),
    typeorm_1.UpdateDateColumn({}),
    __metadata("design:type", Date)
], EntityCoreWithoutId.prototype, "_updatedDate", void 0);
__decorate([
    swagger_1.ApiHideProperty(),
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", Number)
], EntityCoreWithoutId.prototype, "_updatedById", void 0);
__decorate([
    swagger_1.ApiHideProperty(),
    typeorm_1.Column({ nullable: true, length: 40 }),
    __metadata("design:type", String)
], EntityCoreWithoutId.prototype, "_updatedByName", void 0);
__decorate([
    swagger_1.ApiHideProperty(),
    typeorm_1.VersionColumn({}),
    __metadata("design:type", Number)
], EntityCoreWithoutId.prototype, "_version", void 0);
exports.EntityCoreWithoutId = EntityCoreWithoutId;
class EntityCore extends EntityCoreWithoutId {
    static _OPENAPI_METADATA_FACTORY() {
        return { id: { required: false, type: () => Number } };
    }
}
__decorate([
    swagger_1.ApiProperty(),
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], EntityCore.prototype, "id", void 0);
exports.EntityCore = EntityCore;
//# sourceMappingURL=core.entity.js.map
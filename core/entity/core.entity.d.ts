export declare abstract class EntityCoreWithoutId {
    permissionInsert?: string;
    organizationId?: number;
    _createdDate?: Date;
    _createdById?: number;
    _createdByName?: string;
    _updatedDate?: Date;
    _updatedById?: number;
    _updatedByName?: string;
    _version?: number;
}
export declare abstract class EntityCore extends EntityCoreWithoutId {
    id?: number;
}

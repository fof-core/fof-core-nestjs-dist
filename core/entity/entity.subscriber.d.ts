import { EntitySubscriberInterface, InsertEvent, Connection } from 'typeorm';
import { CurrentService } from '../current.service';
import { EntityCore } from './core.entity';
export declare class AllEntitiesSubscriber implements EntitySubscriberInterface {
    readonly connection: Connection;
    private currentService;
    constructor(connection: Connection, currentService: CurrentService);
    /**
     * Called before all entities insertion.
     */
    beforeInsert(event: InsertEvent<EntityCore>): void;
    beforeUpdate(event: InsertEvent<EntityCore>): void;
}

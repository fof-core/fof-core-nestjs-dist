"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const winston = require("winston");
const fof_configuration_1 = require("./fof.configuration");
const logger = winston.createLogger();
exports.logger = logger;
const winstonFormat = winston.format.combine(winston.format.timestamp(), 
// winston.format.colorize(),
winston.format.json(), winston.format.prettyPrint());
const DailyRotateFile = require('winston-daily-rotate-file');
let LoggerService = class LoggerService {
    init(config) {
        const logLevel = fof_configuration_1.fofConfig().debug.level || 'info';
        const serviceName = fof_configuration_1.fofConfig().service.technicalName;
        logger.configure({
            level: logLevel,
            // format: winstonFormat,
            defaultMeta: { service: serviceName },
            transports: [
                new winston.transports.Console(),
                // new winston.transports.File({ filename: `${serviceName}-error.log`, level: 'error' }),
                // new winston.transports.File({ filename: `${serviceName}-combined.log` }),
                new DailyRotateFile({
                    filename: `logs/${serviceName}-error-%DATE%.log`,
                    level: 'error',
                    format: winstonFormat,
                    datePattern: 'YYYY-MM-DD-HH',
                    zippedArchive: true,
                    maxSize: '20m',
                    maxFiles: '14d'
                }),
                new DailyRotateFile({
                    filename: `logs/${serviceName}-combined-%DATE%.log`,
                    datePattern: 'YYYY-MM-DD-HH',
                    zippedArchive: true,
                    maxSize: '20m',
                    maxFiles: '14d'
                })
            ]
        });
        logger.info(`Service started`);
    }
};
LoggerService = __decorate([
    common_1.Injectable()
], LoggerService);
exports.LoggerService = LoggerService;
//# sourceMappingURL=logger.service.js.map
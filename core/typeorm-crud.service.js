"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crud_typeorm_1 = require("@nestjsx/crud-typeorm");
class FofTypeOrmCrudService extends crud_typeorm_1.TypeOrmCrudService {
    constructor(repo) {
        super(repo);
        this.repo = repo;
    }
    async getOne(req) {
        return await super.getOne(req);
    }
    async createOne(req, dto) {
        const user = req.parsed.authPersist;
        const entityCore = dto;
        entityCore._createdById = user.authUserId;
        entityCore._createdByName = user.authUserName;
        entityCore._updatedById = user.authUserId;
        entityCore._updatedByName = user.authUserName;
        return await super.createOne(req, dto);
    }
    async updateOne(req, dto) {
        const user = req.parsed.authPersist;
        const entityCore = dto;
        entityCore._updatedById = user.authUserId;
        entityCore._updatedByName = user.authUserName;
        return await super.updateOne(req, dto);
    }
    async replaceOne(req, dto) {
        const user = req.parsed.authPersist;
        const entityCore = dto;
        entityCore._updatedById = user.authUserId;
        entityCore._updatedByName = user.authUserName;
        return await super.replaceOne(req, dto);
    }
    async createMany(req, dto) {
        const user = req.parsed.authPersist;
        const entitiesCore = dto.bulk;
        entitiesCore.forEach(entityCore => {
            entityCore._createdById = user.authUserId;
            entityCore._createdByName = user.authUserName;
            entityCore._updatedById = user.authUserId;
            entityCore._updatedByName = user.authUserName;
        });
        return await super.createMany(req, dto);
    }
}
exports.FofTypeOrmCrudService = FofTypeOrmCrudService;
//# sourceMappingURL=typeorm-crud.service.js.map
import { Repository } from 'typeorm';
import { CrudRequest, CreateManyDto } from '@nestjsx/crud';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { DeepPartial } from "typeorm";
export declare class FofTypeOrmCrudService<T> extends TypeOrmCrudService<T> {
    protected readonly repo: Repository<T>;
    constructor(repo: Repository<T>);
    getOne(req: CrudRequest): Promise<T>;
    createOne(req: CrudRequest, dto: DeepPartial<T>): Promise<T>;
    updateOne(req: CrudRequest, dto: DeepPartial<T>): Promise<T>;
    replaceOne(req: CrudRequest, dto: DeepPartial<T>): Promise<T>;
    createMany(req: CrudRequest, dto: CreateManyDto<DeepPartial<T>>): Promise<T[]>;
}

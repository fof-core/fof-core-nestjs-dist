"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
const dist_1 = require("typeorm-fixtures-cli/dist");
const typeorm_1 = require("typeorm");
const core_constants_1 = require("../core.constants");
// toDo: remove when in prodction !
exports.loadFixtures = async (fixturesPath) => {
    let connection;
    try {
        connection = typeorm_1.getConnection(core_constants_1.FOF_DEFAULT_CONNECTION);
        const loader = new dist_1.Loader();
        loader.load(path.resolve(fixturesPath));
        const resolver = new dist_1.Resolver();
        const fixtures = resolver.resolve(loader.fixtureConfigs);
        const builder = new dist_1.Builder(connection, new dist_1.Parser());
        let userCpt = 0;
        for (const fixture of dist_1.fixturesIterator(fixtures)) {
            const entity = await builder.build(fixture);
            await typeorm_1.getRepository(entity.constructor.name, core_constants_1.FOF_DEFAULT_CONNECTION).save(entity);
        }
        return { populateIsDone: 1 };
    }
    catch (err) {
        throw err;
    }
};
//# sourceMappingURL=typeorm-fixtures-cli.js.map
export declare const loadFixtures: (fixturesPath: string) => Promise<{
    populateIsDone: number;
}>;

import { CrudOptions } from "@nestjsx/crud";
import { EntityCore } from './entity/core.entity';
import { eCp } from '../shared/permission.enum';
export interface iAllowToJoin {
    name: string;
}
export declare enum eImplementCustom {
    getOne = "getOne",
    getMany = "getMany",
    createOne = "createOne",
    createMany = "createMany",
    updateOne = "updateOne",
    replaceOne = "replaceOne",
    deleteOne = "deleteOne"
}
/** iFofCrudOptions is the short way for setting a controller.
 * For a more advanced usage, have a look to the crudOptions param.
 * @remark
 * All options set in crudOptions will OVERRIDE iFofCrudOptions options
 */
export interface iFofCrudOptions {
    /** typeOrm entity */
    entity: EntityCore | any;
    permissions?: {
        create?: eCp[] | string[];
        read?: eCp[] | string[];
        update?: eCp[] | string[];
        delete?: eCp[] | string[];
    };
    allowToJoin?: iAllowToJoin[];
    implementCustom?: eImplementCustom[];
    excludeFields?: string[];
    /** the original crudOptions
     * @see https://github.com/nestjsx/crud/wiki/Controllers#options
    */
    crudOptions?: CrudOptions;
}
/** Add several decorators and guards in once and set them
 *  @CrudAuth @see https://github.com/nestjsx/crud/wiki/Controllers
 *  @Crud @see https://github.com/nestjsx/crud/wiki/Controllers
 *  @UseGuards FofAuthJwtGuard & FofRBACGuard
 *  @param crudOptions @see https://github.com/nestjsx/crud/wiki/Controllers#options
 */
export declare const FofCrud: (fofCrudOptions?: iFofCrudOptions) => <TFunction extends Function, Y>(target: object | TFunction, propertyKey?: string | symbol, descriptor?: TypedPropertyDescriptor<Y>) => void;
/**
 * Add several Swagger Api decorator in once
 * @ApiBearerAuth
 * @ApiTags
 * @ApiResponse default ones
 */
export declare const FofSwagger: (...tags: string[]) => <TFunction extends Function, Y>(target: object | TFunction, propertyKey?: string | symbol, descriptor?: TypedPropertyDescriptor<Y>) => void;

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var CoreModule_1;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const logger_service_1 = require("./logger.service");
const core_interface_1 = require("./core.interface");
const core_service_1 = require("./core.service");
const permissions_module_1 = require("../permissions/permissions.module");
const core_1 = require("@nestjs/core");
const all_exceptions_filter_1 = require("./all-exceptions.filter");
const rbac_guard_1 = require("../permissions/rbac.guard");
const typeorm_1 = require("@nestjs/typeorm");
const auth_module_1 = require("../auth/auth.module");
const entities_1 = require("../permissions/entities");
const current_service_1 = require("./current.service");
const entity_subscriber_1 = require("./entity/entity.subscriber");
const init_service_1 = require("../init/init.service");
const shared_module_1 = require("../shared/shared.module");
const nestjs_cookie_session_1 = require("nestjs-cookie-session");
const config_1 = require("@nestjs/config");
const fof_configuration_1 = require("./fof.configuration");
const uservices_module_1 = require("../uservices/uservices.module");
const core_constants_1 = require("./core.constants");
const typeorm_2 = require("typeorm");
let CoreModule = CoreModule_1 = class CoreModule {
    constructor() { }
    static forRoot(fofCoreOptions) {
        let envFilePath = [];
        // load additional config files. usufull for monorepo
        if (fofCoreOptions.additionalEndFilePaths && fofCoreOptions.additionalEndFilePaths.length > 0) {
            fofCoreOptions.additionalEndFilePaths.forEach(envFile => {
                envFilePath.push(`.env.${envFile}`);
            });
        }
        // must be in last for being overited it seems
        envFilePath.push('.env');
        const fofModuleOptions = {
            provide: core_interface_1.FOF_CORE_OPTIONS,
            useValue: fofCoreOptions,
        };
        const typeOrmEntitiesCompute = () => {
            let typeOrmEntities;
            typeOrmEntities = [...entities_1.permissionsEntities];
            if (fofCoreOptions.typeOrmModuleOptions) {
                if (fofCoreOptions.typeOrmModuleOptions.entities) {
                    typeOrmEntities = [
                        ...typeOrmEntities,
                        ...fofCoreOptions.typeOrmModuleOptions.entities
                    ];
                }
            }
            return typeOrmEntities;
        };
        const typeOrmEntities = typeOrmEntitiesCompute();
        const typeORMOptions = (appOptions) => {
            const _fofConfig = fof_configuration_1.fofConfig();
            let typeOrmModuleOptions;
            let alreadyConfugured = false;
            if (appOptions) {
                typeOrmModuleOptions = Object.assign(Object.assign({}, appOptions), { entities: typeOrmEntities });
                alreadyConfugured = true;
            }
            if (!alreadyConfugured && fofCoreOptions.typeOrmModuleOptions) {
                typeOrmModuleOptions = Object.assign(Object.assign({}, fofCoreOptions.typeOrmModuleOptions), { entities: typeOrmEntities });
                alreadyConfugured = true;
            }
            if (!alreadyConfugured) {
                typeOrmModuleOptions = {
                    name: core_constants_1.FOF_DEFAULT_CONNECTION,
                    type: 'sqlite',
                    database: `data/${_fofConfig.service.technicalName}-database.db`,
                    dropSchema: _fofConfig.typeORM.dropAndSynchronize,
                    synchronize: _fofConfig.typeORM.dropAndSynchronize,
                    logging: false,
                    subscribers: [],
                    entities: [...typeOrmEntities]
                    // autoLoadEntities: true
                };
            }
            return typeOrmModuleOptions;
        };
        return {
            module: CoreModule_1,
            imports: [
                config_1.ConfigModule.forRoot({
                    isGlobal: true,
                    envFilePath: envFilePath,
                    load: [fof_configuration_1.fofConfig]
                }),
                nestjs_cookie_session_1.CookieSessionModule.forRootAsync({
                    useFactory: async () => {
                        return {
                            session: {
                                name: fof_configuration_1.fofConfig().service.technicalName,
                                secret: fof_configuration_1.fofConfig().secret,
                                path: fof_configuration_1.fofConfig().cookie.path,
                                httpOnly: true
                                // secure: false
                            },
                        };
                    },
                }),
                typeorm_1.TypeOrmModule.forRootAsync({
                    name: core_constants_1.FOF_DEFAULT_CONNECTION,
                    useFactory: async () => {
                        let options;
                        try {
                            options = await typeorm_2.getConnectionOptions('default');
                        }
                        catch (error) {
                            console.log('fof Typeorm: no configuration file founded');
                        }
                        return typeORMOptions(options);
                    }
                }),
                typeorm_1.TypeOrmModule.forFeature([...typeOrmEntities], core_constants_1.FOF_DEFAULT_CONNECTION),
                shared_module_1.SharedModule.forRoot(fofCoreOptions),
                auth_module_1.AuthModule.forRoot(fofCoreOptions),
                permissions_module_1.PermissionsModule.forRoot(fofCoreOptions),
                uservices_module_1.UservicesModule.forRoot(fofCoreOptions),
            ],
            providers: [
                fofModuleOptions,
                logger_service_1.LoggerService,
                core_service_1.CoreService,
                rbac_guard_1.FofRBACGuard,
                {
                    provide: core_1.APP_FILTER,
                    useClass: all_exceptions_filter_1.AllExceptionsFilter,
                },
                current_service_1.CurrentService,
                entity_subscriber_1.AllEntitiesSubscriber,
                init_service_1.InitService
            ],
            exports: [
                logger_service_1.LoggerService,
                rbac_guard_1.FofRBACGuard,
                core_service_1.CoreService,
                permissions_module_1.PermissionsModule,
                fofModuleOptions,
                current_service_1.CurrentService,
                auth_module_1.AuthModule,
                shared_module_1.SharedModule
            ]
        };
    }
};
CoreModule = CoreModule_1 = __decorate([
    common_1.Module({}),
    __metadata("design:paramtypes", [])
], CoreModule);
exports.CoreModule = CoreModule;
//# sourceMappingURL=core.module.js.map
import { ExceptionFilter, ArgumentsHost } from '@nestjs/common';
export interface iUserError {
    id: string;
    email: string;
}
export interface iFofHttpException {
    fofCoreException: boolean;
    errorManaged: boolean;
    status: number;
    statusText?: string;
    timestamp: string;
    url: string;
    user?: iUserError;
    message?: string;
    error?: any;
    ok?: boolean;
}
export declare class AllExceptionsFilter implements ExceptionFilter {
    catch(exception: any, host: ArgumentsHost): void;
}

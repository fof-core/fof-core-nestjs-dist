"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.fofConfig = () => ({
    service: {
        name: process.env.SERVICE_NAME,
        technicalName: process.env.SERVICE_TECHNICAL_NAME
    },
    secret: process.env.SECRET,
    cookie: {
        path: process.env.COOKIE_PATH
    },
    authentication: {
        type: process.env.AUTHENTICATION_TYPE || 'local',
        ldapConnexion: {
            url: process.env.AUTHENTICATION_LDAP_URL,
            base: process.env.AUTHENTICATION_LDAP_BASE,
            bindDN: process.env.AUTHENTICATION_LDAP_BIND_DN,
            bindCredentials: process.env.AUTHENTICATION_LDAP_BIND_CREDENTIAL
        },
    },
    jwt: {
        expiresIn: process.env.JWT_EXPIRES_IN || '1d'
    },
    cacheOptions: {
        /** seconds */
        ttl: parseInt(process.env.CACHE_TTL, 10) || 3600,
        /** maximum number of items in cache  */
        max: parseInt(process.env.CACHE_MAX, 10) || 5000
    },
    debug: {
        level: process.env.DEBUG_LEVEL || 'info',
        consoleDebug: process.env.DEBUG_IN_CONSOLE === 'true'
    },
    rabbitMQ: {
        url: process.env.RABBIT_MQ_URL,
        queue: process.env.RABBIT_MQ_QUEUE
    },
    typeORM: {
        dropAndSynchronize: process.env.TYPEORM_DROP_AND_SYNCHRONIZE === 'true'
    }
});
//# sourceMappingURL=fof.configuration.js.map
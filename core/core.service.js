"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const core_interface_1 = require("./core.interface");
const logger_service_1 = require("./logger.service");
const fof_configuration_1 = require("./fof.configuration");
const uservices_service_1 = require("../uservices/uservices.service");
const uservices_interface_1 = require("../uservices/uservices.interface");
const user_service_1 = require("../permissions/services/user.service");
let CoreService = class CoreService {
    constructor(hConfig, loggerService, fofUservice, userService) {
        this.hConfig = hConfig;
        this.loggerService = loggerService;
        this.fofUservice = fofUservice;
        this.userService = userService;
        const _fofConfig = fof_configuration_1.fofConfig();
        this.fofConfig = fof_configuration_1.fofConfig();
        this.config = hConfig;
        this.loggerService.init(hConfig);
        if (_fofConfig.rabbitMQ.url) {
            this.fofUservice.subscribeTopicMessage([
                uservices_interface_1.efofServiceTopic.fofUservice,
                uservices_interface_1.efofServiceTopic.fofUsers,
                uservices_interface_1.efofServiceTopic.fofUserUservices,
            ], async (msg) => {
                this.manageReceivedMessage(msg);
            });
        }
    }
    manageReceivedMessage(msg) {
        switch (msg.topic) {
            case uservices_interface_1.efofServiceTopic.fofUsers:
                if (!this.config.isMasterForMiniServicesManagement) {
                    // console.log('user topic')
                    return this.userService.uServiceMessageManage(msg);
                }
                break;
            case uservices_interface_1.efofServiceTopic.fofUservice:
                if (!this.config.isMasterForMiniServicesManagement) {
                    // console.log('mini service topic')
                }
                break;
            case uservices_interface_1.efofServiceTopic.fofUserUservices:
                if (!this.config.isMasterForUserManagement) {
                    // console.log('mini service user')
                }
                break;
            default:
                break;
        }
    }
};
CoreService = __decorate([
    common_1.Injectable(),
    __param(0, common_1.Inject(core_interface_1.FOF_CORE_OPTIONS)),
    __metadata("design:paramtypes", [Object, logger_service_1.LoggerService,
        uservices_service_1.FofUservice,
        user_service_1.UserService])
], CoreService);
exports.CoreService = CoreService;
//# sourceMappingURL=core.service.js.map
import { DynamicModule } from '@nestjs/common';
import { FofHardcodedOptions } from './core.interface';
export declare class CoreModule {
    constructor();
    static forRoot(fofCoreOptions: FofHardcodedOptions): DynamicModule;
}

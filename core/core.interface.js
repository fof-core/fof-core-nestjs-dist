"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/** Authentication type.
 * Daefult is basic
 */
var eAuth;
(function (eAuth) {
    eAuth["basic"] = "basic";
    eAuth["windows"] = "windows";
})(eAuth = exports.eAuth || (exports.eAuth = {}));
/** for internal use only */
exports.FOF_CORE_OPTIONS = 'FofHardcodedOptions';
/** Implemented in core search. Should be implemented in all search with pagination */
class iSearchResult {
}
exports.iSearchResult = iSearchResult;
//# sourceMappingURL=core.interface.js.map
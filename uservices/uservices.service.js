"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const nestjs_rabbitmq_1 = require("@golevelup/nestjs-rabbitmq");
const fof_configuration_1 = require("../core/fof.configuration");
const core_interface_1 = require("../core/core.interface");
const uservices_interface_1 = require("./uservices.interface");
exports.fofNack = nestjs_rabbitmq_1.Nack;
let FofUservice = class FofUservice {
    constructor(config, amqpConnection) {
        this.config = config;
        this.amqpConnection = amqpConnection;
        this._serviceTechnicalName = fof_configuration_1.fofConfig().service.technicalName;
    }
    async publishTopicMessage(dto, topic, method) {
        const message = {
            dto: dto,
            method: method,
            //toDo: change for getting rbtmq topic instead
            topic: topic,
            publisher: this._serviceTechnicalName
        };
        const options = {
            persistent: true
        };
        try {
            await this.amqpConnection.publish(uservices_interface_1.FOF_USERVICE_CHANNEL, topic, message, options);
        }
        catch (error) {
            // if connexion not avilable, keep to send one more time when the connexion will be up
            console.log('toDo error publish', error);
        }
        return true;
    }
    async subscribeTopicMessage(topics, handler) {
        const options = {
            type: 'subscribe',
            exchange: uservices_interface_1.FOF_USERVICE_CHANNEL,
            routingKey: topics,
            queue: this._serviceTechnicalName
        };
        return await this.amqpConnection.createSubscriber(handler, options);
    }
};
FofUservice = __decorate([
    common_1.Injectable(),
    __param(0, common_1.Inject(core_interface_1.FOF_CORE_OPTIONS)),
    __metadata("design:paramtypes", [Object, nestjs_rabbitmq_1.AmqpConnection])
], FofUservice);
exports.FofUservice = FofUservice;
//# sourceMappingURL=uservices.service.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var efofServiceTopic;
(function (efofServiceTopic) {
    efofServiceTopic["fofUsers"] = "fof.user";
    efofServiceTopic["fofOrganizations"] = "fof.organization";
    efofServiceTopic["fofUservice"] = "fof.uservice";
    efofServiceTopic["fofUserUservices"] = "fof.user.uservice";
})(efofServiceTopic = exports.efofServiceTopic || (exports.efofServiceTopic = {}));
var eFofServiceMethod;
(function (eFofServiceMethod) {
    eFofServiceMethod["create"] = "create";
    eFofServiceMethod["read"] = "read";
    eFofServiceMethod["update"] = "update";
    eFofServiceMethod["delete"] = "delete";
})(eFofServiceMethod = exports.eFofServiceMethod || (exports.eFofServiceMethod = {}));
exports.FOF_USERVICE_CHANNEL = 'fof-uservice-channel';
//# sourceMappingURL=uservices.interface.js.map
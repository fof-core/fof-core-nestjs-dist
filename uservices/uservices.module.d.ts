import { DynamicModule, OnModuleInit } from '@nestjs/common';
import { FofHardcodedOptions } from '../core/core.interface';
export declare class UservicesModule implements OnModuleInit {
    onModuleInit(): void;
    static forRoot(options: FofHardcodedOptions): DynamicModule;
}

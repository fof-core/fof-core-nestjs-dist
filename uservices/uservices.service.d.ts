import { Nack, SubscribeResponse, AmqpConnection } from '@golevelup/nestjs-rabbitmq';
import { FofHardcodedOptions } from '../core/core.interface';
import { eUsMessage, eFofServiceMethod } from './uservices.interface';
import { ConsumeMessage } from 'amqplib';
export declare const fofNack: typeof Nack;
export declare class FofUservice {
    private config;
    private amqpConnection;
    constructor(config: FofHardcodedOptions, amqpConnection: AmqpConnection);
    private _serviceTechnicalName;
    publishTopicMessage(dto: any, topic: string, method?: eFofServiceMethod | string): Promise<boolean>;
    subscribeTopicMessage(topics: string[], handler: (msg: eUsMessage, rawMessage?: ConsumeMessage) => Promise<SubscribeResponse | void>): Promise<void>;
}

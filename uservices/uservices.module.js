"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var UservicesModule_1;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const core_interface_1 = require("../core/core.interface");
const uservices_controller_1 = require("../permissions/controllers/uservices.controller");
const uservices_service_1 = require("./uservices.service");
const nestjs_rabbitmq_1 = require("@golevelup/nestjs-rabbitmq");
const uservices_interface_1 = require("./uservices.interface");
const uservice_service_1 = require("../permissions/services/uservice.service");
const fof_configuration_1 = require("../core/fof.configuration");
const typeorm_1 = require("@nestjs/typeorm");
const core_constants_1 = require("../core/core.constants");
const uservice_entity_1 = require("../permissions/entities/uservice.entity");
let UservicesModule = UservicesModule_1 = class UservicesModule {
    onModuleInit() {
        // console.log(`The module has been initialized.`);
    }
    static forRoot(options) {
        const logger = new common_1.Logger(UservicesModule_1.name);
        // logger.log('Yop fof')
        const typeModuleOptions = {
            provide: core_interface_1.FOF_CORE_OPTIONS,
            useValue: options,
        };
        return {
            module: UservicesModule_1,
            imports: [
                typeorm_1.TypeOrmModule.forFeature([
                    uservice_entity_1.FofuService
                ], core_constants_1.FOF_DEFAULT_CONNECTION),
                nestjs_rabbitmq_1.RabbitMQModule.forRootAsync(nestjs_rabbitmq_1.RabbitMQModule, {
                    useFactory: async () => {
                        return {
                            exchanges: [{
                                    name: uservices_interface_1.FOF_USERVICE_CHANNEL,
                                    type: 'topic'
                                }],
                            uri: fof_configuration_1.fofConfig().rabbitMQ.url,
                            connectionInitOptions: { wait: false },
                            registerHandlers: true
                        };
                    }
                })
            ],
            providers: [
                typeModuleOptions,
                uservices_service_1.FofUservice,
                uservice_service_1.UserviceCrudService
            ],
            controllers: [
                uservices_controller_1.UservicesController
            ],
            exports: [
                nestjs_rabbitmq_1.RabbitMQModule,
                uservices_service_1.FofUservice
            ]
        };
    }
};
UservicesModule = UservicesModule_1 = __decorate([
    common_1.Module({})
], UservicesModule);
exports.UservicesModule = UservicesModule;
//# sourceMappingURL=uservices.module.js.map
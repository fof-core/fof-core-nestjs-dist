export declare enum efofServiceTopic {
    fofUsers = "fof.user",
    fofOrganizations = "fof.organization",
    fofUservice = "fof.uservice",
    fofUserUservices = "fof.user.uservice"
}
export declare enum eFofServiceMethod {
    create = "create",
    read = "read",
    update = "update",
    delete = "delete"
}
export declare const FOF_USERVICE_CHANNEL = "fof-uservice-channel";
export interface eUsMessage {
    publisher: string;
    method?: eFofServiceMethod | string;
    topic?: string;
    dto?: any;
}

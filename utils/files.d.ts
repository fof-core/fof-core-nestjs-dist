export declare module files {
    /**
     * Synchronously reads the entire contents of a UTF8 file.
     * @param path A path to a file. If a URL is provided, it must use the `file:` protocol.
     *
     * @remarks
     * URL support is _experimental_.
     * If a file descriptor is provided, the underlying file will _not_ be closed automatically.
     */
    const readSync: (filePath: string) => string;
    const exist: (filePath: string) => Promise<boolean>;
    /**
     * Ensure the file exist and create the path if necessary.
     * @param filePath relative path from the process root (e.g. logs/new.log)
     */
    const ensureFile: (filePath: string) => Promise<boolean>;
    /**
     * Asynchronously write the content to a file with UTF8 format.
     * @param filePath The file path. If an URL is provided, it must use the `file:` protocol.
     * @param fileContent The content to write in the file path (in UTF8).
     * @returns filePath & fileContent
     *
     * @remarks
     * URL support is _experimental_.
     * If a file descriptor is provided, the underlying file will _not_ be closed automatically.
     */
    const write: (filePath: string, fileContent: string) => Promise<any>;
}

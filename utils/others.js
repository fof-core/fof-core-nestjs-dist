"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var utils;
(function (utils) {
    utils.uuid = () => {
        const uuidv1 = require('uuid/v1');
        return uuidv1();
    };
    utils.forEachAsync = async (array, callback) => {
        for (let index = 0; index < array.length; index++) {
            await callback(array[index], index, array);
        }
    };
})(utils = exports.utils || (exports.utils = {}));
//# sourceMappingURL=others.js.map
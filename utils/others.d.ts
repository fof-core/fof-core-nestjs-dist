export declare module utils {
    const uuid: () => string;
    const forEachAsync: (array: any[], callback: any) => Promise<void>;
}

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const path = require("path");
const fsExtra = require("node-fs-extra");
const rootPath = process.cwd();
var files;
(function (files) {
    /**
     * Synchronously reads the entire contents of a UTF8 file.
     * @param path A path to a file. If a URL is provided, it must use the `file:` protocol.
     *
     * @remarks
     * URL support is _experimental_.
     * If a file descriptor is provided, the underlying file will _not_ be closed automatically.
     */
    //todo: remove the sync part!!
    files.readSync = (filePath) => {
        try {
            const fileRootPath = path.join(`${rootPath}/`, filePath);
            return fs.readFileSync(filePath, 'utf8');
        }
        catch (error) {
            throw error;
        }
    };
    files.exist = (filePath) => {
        return new Promise(function (resolve, reject) {
            fs.access(filePath, (err) => {
                if (err) {
                    resolve(false);
                }
                else {
                    // file exists
                    resolve(true);
                }
            });
        });
    };
    /**
     * Ensure the file exist and create the path if necessary.
     * @param filePath relative path from the process root (e.g. logs/new.log)
     */
    files.ensureFile = (filePath) => {
        return new Promise(function (resolve, reject) {
            const fullFilePath = path.join(`${rootPath}/`, filePath);
            fsExtra.ensureFile(fullFilePath)
                .then(() => {
                resolve(true);
            })
                .catch(reason => {
                reject(reason);
            });
        });
    };
    /**
     * Asynchronously write the content to a file with UTF8 format.
     * @param filePath The file path. If an URL is provided, it must use the `file:` protocol.
     * @param fileContent The content to write in the file path (in UTF8).
     * @returns filePath & fileContent
     *
     * @remarks
     * URL support is _experimental_.
     * If a file descriptor is provided, the underlying file will _not_ be closed automatically.
     */
    files.write = (filePath, fileContent) => {
        return new Promise(function (resolve, reject) {
            // console.log()
            fs.writeFile(`${rootPath}/${filePath}`, fileContent, 'utf8', (error) => {
                if (error) {
                    reject(error);
                }
                else {
                    resolve({
                        filePath: filePath,
                        fileContent: fileContent
                    });
                }
            });
        });
    };
})(files = exports.files || (exports.files = {}));
//# sourceMappingURL=files.js.map
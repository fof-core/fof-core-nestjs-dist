"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var SharedModule_1;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const cache_service_1 = require("./cache.service");
const fof_configuration_1 = require("../core/fof.configuration");
let SharedModule = SharedModule_1 = class SharedModule {
    static forRoot(options) {
        const cacheModuleOptions = {
            // seconds
            ttl: fof_configuration_1.fofConfig().cacheOptions.ttl,
            // maximum number of items in cache 
            max: fof_configuration_1.fofConfig().cacheOptions.max,
        };
        return {
            module: SharedModule_1,
            global: true,
            imports: [
                common_1.CacheModule.register(cacheModuleOptions),
            ],
            providers: [
                cache_service_1.CacheService
            ],
            exports: [
                cache_service_1.CacheService
            ]
        };
    }
};
SharedModule = SharedModule_1 = __decorate([
    common_1.Module({})
], SharedModule);
exports.SharedModule = SharedModule;
//# sourceMappingURL=shared.module.js.map
export declare class iUser {
    id?: number;
    windowAuthentification?: boolean;
    email?: string;
    login?: string;
    permissions?: Array<string>;
    firstName?: string;
    lastName?: string;
    fullName?: string;
    accessToken?: string;
}
export declare class iUserAuth {
    username: string;
    password: string;
}

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var eCp;
(function (eCp) {
    eCp["userCreate"] = "userCreate";
    eCp["userUpdate"] = "userUpdate";
    eCp["userRead"] = "userRead";
    eCp["userDelete"] = "userDelete";
    eCp["roleCreate"] = "roleCreate";
    eCp["roleUpdate"] = "roleUpdate";
    eCp["roleRead"] = "roleRead";
    eCp["roleDelete"] = "roleDelete";
    eCp["userRoleCreate"] = "userRoleCreate";
    eCp["userRoleUpdate"] = "userRoleUpdate";
    eCp["userRoleRead"] = "userRoleRead";
    eCp["userRoleDelete"] = "userRoleDelete";
    eCp["organizationCreate"] = "organizationCreate";
    eCp["organizationRead"] = "organizationRead";
    eCp["organizationUpdate"] = "organizationUpdate";
    eCp["organizationDelete"] = "organizationDelete";
    eCp["coreAdminAccess"] = "coreAdminAccess";
})(eCp = exports.eCp || (exports.eCp = {}));
//# sourceMappingURL=permission.enum.js.map
export interface ICacheManager {
    store: any;
    get(key: string): any;
    set(key: string, value: string, options?: {
        ttl: number;
    }): any;
    del(key: string): any;
}
export declare class CacheService {
    private cache;
    constructor(cache: ICacheManager);
    get(key: string): Promise<any>;
    set(key: string, value: any, options?: {
        ttl: number;
    }): Promise<any>;
    del(key: string): Promise<any>;
}

export declare enum eCp {
    userCreate = "userCreate",
    userUpdate = "userUpdate",
    userRead = "userRead",
    userDelete = "userDelete",
    roleCreate = "roleCreate",
    roleUpdate = "roleUpdate",
    roleRead = "roleRead",
    roleDelete = "roleDelete",
    userRoleCreate = "userRoleCreate",
    userRoleUpdate = "userRoleUpdate",
    userRoleRead = "userRoleRead",
    userRoleDelete = "userRoleDelete",
    organizationCreate = "organizationCreate",
    organizationRead = "organizationRead",
    organizationUpdate = "organizationUpdate",
    organizationDelete = "organizationDelete",
    coreAdminAccess = "coreAdminAccess"
}
